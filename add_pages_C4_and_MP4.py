import os
import sys
import csv
import xlrd
import helpers as h

def get_string(sheet, row, col):
    try:
        cell = sheet.cell(row, col)
    except IndexError:
        return None
        # print(sheet.cell(row, col-1))
        # print(dir(sheet), row, col)
        # raise
    v = cell.value
    if cell.ctype == 3:
        print("%s%d"%(chr(ord("A") + col), row+1), repr(v))
        raise Exception("DATE type found")
    v = str(v).strip()
    if col not in [1, 20]:
        v = v.lower()
    if v == "":
        return None
    v = " ".join(v.split())
    v = ", ".join([word.strip() for word in v.split(",")])
    return v

def get_row(sheet, row):
    return [get_string(sheet, row, col) for col in range(sheet.ncols)]

def main():
    fname1 = sys.argv[1]
    fname1_abs = os.path.abspath(fname1)
    fname1_base = os.path.basename(fname1_abs)
    fname1_new = fname1_abs + ".csv"

    wb1 = xlrd.open_workbook(fname1_abs)
    sheet1 = wb1.sheets()[0]

    fname2 = sys.argv[2]
    fname2_abs = os.path.abspath(fname2)
    fname2_base = os.path.basename(fname2_abs)

    wb2 = xlrd.open_workbook(fname2_abs)
    sheet2 = wb2.sheets()[0]

    # print(dir(sheet))

    row = 2
    pages = 0
    rows_new = []
    while row < sheet2.nrows:
        row += 1

        values = get_row(sheet1, row-pages)
        page_row = get_row(sheet2, row)
        page = page_row[0]
        if page not in [None, "broj"]:
            print(page, row)
            if page.lower().find("str") == -1:
                raise Exception("The A cell for row %d should be a Page cell."%(row+1))
            if any(page_row[2:]):
                print(page_row[2:])
                raise Exception("There should be no values after B for row %d."%(row+1))
            if page_row[1] is None:
                page_row_previous = get_row(sheet2, row-1)[2:4]
                values_previous = get_row(sheet1, row-pages-1)[2:4]
                page_row_next = get_row(sheet2, row+1)[2:4]
                if page_row_previous == values_previous and \
                    page_row_next == values[2:4]:
                    page_num = int(page[3:].strip())
                    page_row[19] = "Str %d"%(page_num - 1)
                    rows_new.append(page_row)
                    pages += 1
                else:
                    print(page_row_previous)
                    print(values_previous)
                    print(page_row_next)
                    print(values[2:4])
                    raise Exception("Non matching pre and post row for row %d/%d."%(row+1, row-pages+1))
            else:
                if page_row[2:4] == values[2:4]:
                    values[0] = page
                    page_num = int(page[3:].strip())
                    values[19] = "Str %d"%(page_num - 1)
                else:
                    raise Exception("Non matching pre and post row for row %d/%d."%(row+1, row-pages+1))
        else:
            if page_row[2:4] != values[2:4]:
                print(page_row[2:4])
                print(values[2:4])
                raise Exception("Non matching rows %d/%d."%(row+1, row-pages+1))
        rows_new.append(values)

    with open(fname1_new, 'w', encoding='utf8') as csvfile:
        csvwriter = csv.writer(csvfile, lineterminator='\n')
        for row in rows_new:
            row_new = [h.myconvert(e) for e in row]
            csvwriter.writerow(row_new)

if __name__ == "__main__":
    main()