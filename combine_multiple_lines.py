import sys
import xlrd
import csv

from parse_mechanical import COLUMNS

wb = xlrd.open_workbook(sys.argv[1], formatting_info=True)
s = wb.sheets()[0]

rows = []

row_ris = []
for ri in range(s.nrows):
    c = s.cell(ri, 0)
    fmt = wb.xf_list[c.xf_index]
    # if fmt.border.top_colour_index == 0 and fmt.border.bottom_colour_index == 0:
    #     fmt.dump()
    #     print("Finished first pass... at row %d"%ri)
    #     break
    # else:
    if fmt.border.bottom_colour_index == 0:
        row_ris.append(ri)
    elif fmt.border.bottom_colour_index == 8:
        row_ris.append(ri)
        rows.append(row_ris)
        row_ris = []
    else:
        raise Exception("Row number %d has a problematic border"%ri)
    if len(row_ris) > 2:
        raise Exception("Row number %d is the third cell in a row"%ri)

print(rows)
def myconvert(e):
    if e is not None:
        return str(e)
    else:
        return ""

with open(sys.argv[1] + ".csv", 'w', encoding='utf8') as csvfile:
    csvwriter = csv.writer(csvfile)
    for i, row_ris in enumerate(rows):
        row = []
        for ci in range(len(COLUMNS.items())):
            value = ""
            cell_members = []
            for ri in row_ris:
                try:
                    _v = myconvert(s.cell(ri, ci).value)
                    if _v:
                        cell_members.append(_v)
                except IndexError:
                    pass
            value = " ".join(cell_members)
            row.append(value)
        csvwriter.writerow(row)