python coordinates_check_consistency.py "Coordinates/coordinates, str 001-001, MapInfo.xls" && \
python parse_coordinates.py Coordinates -f && \
python coordinates_fix_profile.py Coordinates.sqlite -f && \

python parse_mechanical.py MP_1 -b 7 -e 172 -f && \
python fix_profile.py MP_1.sqlite -f && \
python parse_mechanical.py MP_2 -b 1 -e 91 -f && \
python fix_profile.py MP_2.sqlite -f && \
python parse_mechanical.py MP_3 -b 1 -e 173 -f && \
python fix_profile.py MP_3.sqlite -f && \
python mergesqlitefiles.py MP_1.fix_profile.sqlite MP_2.fix_profile.sqlite MP_3.fix_profile.sqlite -o MP_123.fix_profile.sqlite -f && \
python add_xy.py MP_123.fix_profile.sqlite Coordinates.fix_profile.sqlite && \

python parse_chemical.py C_1 -b 7 -e 173 -f && \
python fix_profile.py C_1.sqlite -f && \
python parse_chemical.py C_2 -b 1 -e 163 -f && \
python fix_profile.py C_2.sqlite -f && \
python parse_chemical.py C_3 -b 1 -e 136 -f && \
python fix_profile.py C_3.sqlite -f && \
python mergesqlitefiles.py C_1.fix_profile.sqlite C_2.fix_profile.sqlite C_3.fix_profile.sqlite -o C_123.fix_profile.sqlite -f && \
python add_xy.py C_123.fix_profile.sqlite Coordinates.fix_profile.sqlite && \

python join_MP_and_C.py MP_123.fix_profile.add_xy.sqlite C_123.fix_profile.add_xy.sqlite  -o MP_C_123.join_by_xy.sqlite && \

python parse_mechanical.py MP_5 -f && \
python fix_profile.py MP_5.sqlite -f && \
python add_xy.py MP_5.fix_profile.sqlite Coordinates.fix_profile.sqlite && \

python parse_chemical.py C_5 -f && \
python fix_profile.py C_5.sqlite -f && \
python add_xy.py C_5.fix_profile.sqlite Coordinates.fix_profile.sqlite && \

python join_MP_and_C.py MP_5.fix_profile.add_xy.sqlite C_5.fix_profile.add_xy.sqlite  --handler=handler_plain_join -o MP_C_5.join_by_xy.sqlite && \

python parse_mechanical.py MP_4 -f && \
python fix_profile.py MP_4.sqlite -f && \
python add_xy.py MP_4.fix_profile.sqlite Coordinates.fix_profile.sqlite && \

python parse_chemical.py C_4 -f && \
python fix_profile.py C_4.sqlite -f && \
python add_xy.py C_4.fix_profile.sqlite Coordinates.fix_profile.sqlite && \

python join_MP_and_C.py MP_4.fix_profile.add_xy.sqlite C_4.fix_profile.add_xy.sqlite --handler=handler_plain_join -o MP_C_4.join_by_xy.sqlite && \

# Adding the "final" x, y coordinates
python add_xy.py MP_C_123.join_by_xy.sqlite Coordinates.fix_profile.sqlite --save-non-matched-coordinates && \
python add_xy.py MP_C_5.join_by_xy.sqlite non_matched_coordinates.sqlite --save-non-matched-coordinates && \
python add_xy.py MP_C_4.join_by_xy.sqlite non_matched_coordinates.sqlite --save-non-matched-coordinates && \

python mergesqlitefiles.py MP_C_123.join_by_xy.add_xy.sqlite MP_C_5.join_by_xy.add_xy.sqlite MP_C_4.join_by_xy.add_xy.sqlite -o MP_C_12354.sqlite -f && \
# python sqlite2sqlite.py MP_C_12354.sqlite --sql "select * from properties where x is not null" -o MP_C_12354.xy_not_null.sqlite && \

python add_xy_view.py MP_C_12354.sqlite -f && \
python add_xy_view.py MP_C_4.join_by_xy.sqlite -f && \
cp MP_C_12354.sqlite MP_C_.final.sqlite && \
python add_xy.py MP_C_4.join_by_xy.sqlite  Coordinates.fix_profile.sqlite -o MP_C_4.final.sqlite && \
python sqlite2csv.py MP_C_.final.sqlite -o MP_C_.final.csv && \
python sqlite2csv.py MP_C_.final.sqlite --table=xy_view -o MP_C_.final.xy_view.csv && \
python sqlite2csv.py MP_C_4.final.sqlite -o MP_C_4.final.csv && \
python sqlite2csv.py MP_C_4.final.sqlite --table=xy_view -o MP_C_4.final.xy_view.csv && \
libreoffice --headless --convert-to xls --infilter=CSV:44,34,UTF8 MP_C_.final.csv && \
libreoffice --headless --convert-to xls --infilter=CSV:44,34,UTF8 MP_C_.final.xy_view.csv && \
libreoffice --headless --convert-to xls --infilter=CSV:44,34,UTF8 MP_C_4.final.csv && \
libreoffice --headless --convert-to xls --infilter=CSV:44,34,UTF8 MP_C_4.final.xy_view.csv && \
#python add_dem.py MP_C_.final.sqlite -f && \

python sqlite2csv.py MP_C_.final.sqlite -s"select x, y, MP_my_profile_id, MP_depth_max - MP_depth_min total_depth, C_P2O5_value p2o5 from (select * from properties where x is not null and C_P2O5_value is not null and C_P2O5_value<39 and MP_depth_min=0)" -o p2o5_xy.csv && \
python create_kriging_grid.py p2o5_xy.csv -f && \
# python sqlite2csv.py MP_C_.final.sqlite -s"select MP_my_profile_id, x, y, elevation, slope, aspect, plan_curvature, MP_depth_max - MP_depth_min total_depth, C_P2O5_value p2o5 from (select * from xy_view where C_P2O5_value is not null and MP_depth_min=0)" -o p2o5_xy.csv && \
# python create_kriging_grid.py p2o5_xy.csv -f && \


# python fix_profile.py MP_123.sqlite -f && \
# python check_lab_no.py MP_123.sqlite -f && \

# python fix_profile.py C_123.sqlite -f && \
# python check_lab_no.py C_123.sqlite -f && \

# python mergesqlitefiles.py C_123.fix_profile.sqlite C_5.fix_profile.sqlite -o C_1235.fix_profile.sqlite -f

# python sqlite2sqlite.py MP_123.fix_profile.sqlite --sql "select * from properties where section is not null" -o MP_123.section_not_null.sqlite && \
# python sqlite2sqlite.py C_123.fix_profile.sqlite --sql "select * from properties where section is not null" -o C_123.section_not_null.sqlite && \
# python join_MP_and_C.py --handler=handler_join_by_section MP_123.section_not_null.sqlite C_123.section_not_null.sqlite -o MP_C_123.section_not_null.sqlite -f && \

# python sqlite2sqlite.py MP_123.fix_profile.sqlite --sql "select * from properties where section is null" -o MP_123.section_null.sqlite && \
# python sqlite2sqlite.py C_123.fix_profile.sqlite --sql "select * from properties where section is null" -o C_123.section_null.sqlite && \
# python join_MP_and_C.py MP_123.section_null.sqlite C_123.section_null.sqlite --handler=handler_non_thorough -o MP_C_123.section_null.sqlite && \

# python join_MP_and_C.py MP_123.fix_profile.sqlite C_123.fix_profile.sqlite -o MP_C_123.sqlite -f | tee merging_out.txt && \
# python join_MP_and_C.py MP_4.fix_profile.sqlite C_4.fix_profile.sqlite --handler=handler_plain_join -o MP_C_4.sqlite -f && \
# python join_MP_and_C.py MP_5.fix_profile.sqlite C_5.fix_profile.sqlite --handler=handler_plain_join -o MP_C_5.sqlite -f && \
# python mergesqlitefiles.py MP_C_123.sqlite MP_C_5.sqlite -o MP_C_.sqlite -f && \

# python add_xy.py MP_C_.sqlite Coordinates.fix_profile.sqlite -f # REM | tee coordinates_matching_output.txt
# python add_dem.py MP_C_.add_xy.sqlite -f && \
# python add_xy_view.py MP_C_.add_xy.add_dem.sqlite -f && \
# cp MP_C_.add_xy.add_dem.sqlite MP_C_.final.sqlite

# python add_xy.py MP_C_123.section_not_null.sqlite Coordinates.fix_profile.sqlite -f --save-non-matched-coordinates && \
# python sqlite2csv.py MP_C_123.section_not_null.add_xy.sqlite --sql "select * from properties where x is not null" -o MP_C_123.section_not_null.x_not_null.csv && \

# python guess_sections_for_MP_C_5.py MP_C_5.sqlite non_matched_coordinates.sqlite -o MP_C_5.section_not_null.sqlite -f && \
# python add_xy.py MP_C_5.section_guessed.sqlite non_matched_coordinates.sqlite --save-non-matched-coordinates -f && \
# python sqlite2csv.py MP_C_5.section_guessed.add_xy.sqlite --sql "select * from properties where x is not null" -o MP_C_5.section_not_null.x_not_null.csv -f && \

# python add_xy.py MP_C_4.sqlite non_matched_coordinates.sqlite -f && \
# python sqlite2csv.py MP_C_4.add_xy.sqlite --sql "select * from properties where x is not null" -o MP_C_4.section_not_null.x_not_null.csv && \

# Create sqlite files only with profiles that have x and y set
# python sqlite2sqlite.py MP_C_123.section_not_null.add_xy.sqlite --sql "select * from properties where x is not null" -o MP_C_123.section_not_null.x_not_null.sqlite && python sqlite2sqlite.py MP_C_5.section_guessed.add_xy.sqlite --sql "select * from properties where x is not null" -o MP_C_5.section_not_null.x_not_null.sqlite -f && python sqlite2sqlite.py MP_C_4.add_xy.sqlite --sql "select * from properties where x is not null" -o MP_C_4.section_not_null.x_not_null.sqlite && \
# python mergesqlitefiles.py MP_C_123.section_not_null.x_not_null.sqlite MP_C_5.section_not_null.x_not_null.sqlite MP_C_4.section_not_null.x_not_null.sqlite -o MP_C_123_5_4.section_not_null.x_not_null.sqlite -f && \
# python add_dem.py MP_C_123_5_4.section_not_null.x_not_null.sqlite -f && \
# python add_xy_view.py MP_C_123_5_4.section_not_null.x_not_null.add_dem.sqlite -f && \
# cp MP_C_123_5_4.section_not_null.x_not_null.add_dem.sqlite MP_C_.final.sqlite && \

# python mergesqlitefiles.py MP_123.fix_profile.sqlite MP_5.fix_profile.sqlite -o MP_1235.fix_profile.sqlite -f

# To be used to count the number of profiles that have x, y
# python sqlite2csv.py MP_C_123.section_not_null.add_xy.sqlite --sql "select distinct MP_my_profile_id, MP_section, x, y from properties where x is not null" -o MP_C_123.section_not_null.x_not_null.csv && python sqlite2csv.py MP_C_5.section_guessed.add_xy.sqlite --sql "select distinct MP_my_profile_id, MP_section, x, y from properties where x is not null" -o MP_C_5.section_not_null.x_not_null.csv -f && python sqlite2csv.py MP_C_4.add_xy.sqlite --sql "select distinct MP_my_profile_id, MP_section, x, y from properties where x is not null" -o MP_C_4.section_not_null.x_not_null.csv -f

# python sqlite2csv.py MP_C_123.section_not_null.add_xy.sqlite --sql "select * from properties where x is not null" -o MP_C_123.section_not_null.x_not_null.csv && python sqlite2csv.py MP_C_5.section_guessed.add_xy.sqlite --sql "select * from properties where x is not null" -o MP_C_5.section_not_null.x_not_null.csv -f && python sqlite2csv.py MP_C_4.add_xy.sqlite --sql "select * from properties where x is not null" -o MP_C_4.section_not_null.x_not_null.csv

# python sqlite2csv.py MP_C_.final.sqlite -s"select MP_my_profile_id, x, y, elevation, max(MP_depth_max) - min(MP_depth_min) total_depth, sum(C_P2O5_value) P2O5_total, sum(C_P2O5_value)/(max(MP_depth_max) - min(MP_depth_min)) P2O5_per_cm from (select * from xy_view where C_P2O5_value is not null) group by MP_my_profile_id" -o p2o5_xy.csv
# python sqlite2csv.py MP_C_.final.sqlite -s"select MP_my_profile_id, x, y, elevation, slope, aspect, plan_curvature, max(MP_depth_max) - min(MP_depth_min) total_depth, sum(C_P2O5_value) P2O5_total, sum(C_P2O5_value)/(max(MP_depth_max) - min(MP_depth_min)) P2O5_per_cm from (select * from xy_view where C_P2O5_value is not null) group by MP_my_profile_id" -o p2o5_xy.csv
# python sqlite2csv.py MP_C_.final.sqlite -s"select MP_my_profile_id, x, y, elevation, slope, aspect, plan_curvature, MP_depth_max - MP_depth_min total_depth, C_P2O5_value p2o5 from (select * from xy_view where C_P2O5_value is not null and MP_depth_min=0)" -o p2o5_xy.csv
# python create_kriging_grid.py p2o5_xy.csv -f

# Kriging
# gdalwarp /media/Evo850-1TB-1/edin/afh/BIO-ICT/Mirko/GLS1990/p187r031_5dt19870724_z34_10.tif /media/Evo850-1TB-1/edin/afh/me_soil_tables/GLS1990_Edin_CRS_p187r031_5dt19870724_z34_10.tif -t_srs "+proj=tmerc +lat_0=0 +lon_0=18 +k=0.9999 +x_0=6500000 +y_0=0 +ellps=bessel +towgs84=750,-230,580,0,0,0,0 +units=m +no_defs"
# gdalwarp /media/Evo850-1TB-1/edin/afh/BIO-ICT/Mirko/GLS1975/p201r030_3dm19780706_z34_10.tif /media/Evo850-1TB-1/edin/afh/me_soil_tables/GLS1975_Edin_CRS_p201r030_3dm19780706_z34_10.tif -t_srs "+proj=tmerc +lat_0=0 +lon_0=18 +k=0.9999 +x_0=6500000 +y_0=0 +ellps=bessel +towgs84=750,-230,580,0,0,0,0 +units=m +no_defs"
# gdalwarp e:\Data\edin\afh\BIO-IC_T\Mirko\GMTED2010N30E000_075\30n000e_20101117_gmted_mea075.tif Edin_30n000e_20101117_gmted_mea075.tif -t_srs "+proj=tmerc +lat_0=0 +lon_0=18 +k=0.9999 +x_0=6500000 +y_0=0 +ellps=bessel +towgs84=750,-230,580,0,0,0,0 +units=m +no_defs"
# gdalwarp e:\Data\edin\afh\BIO-IC_T\Mirko\K34\Montenegro_K34.tif e:\Data\edin\afh\me_soil_tables\Montenegro_K34_Edin_CRS_DEM_elevation.tif -t_srs "+proj=tmerc +lat_0=0 +lon_0=18 +k=0.9999 +x_0=6500000 +y_0=0 +ellps=bessel +towgs84=750,-250,480,0,0,0,0 +units=m +no_defs"
# gdalwarp -q -cutline e:/Data/edin/afh/BIO-ICT/Mirko/Cetinje1.shp -crop_to_cutline -of GTiff e:\Data\edin\afh\me_soil_tables\Montenegro_K34_Edin_CRS_DEM_elevation.tif E:/Data/edin/afh/BIO-ICT/Mirko/Cetinje1_DEM.tif

# Data mining
python rm_sign.py MP_C_.final.sqlite -f && \
python categories2numbers.py MP_C_.final.rm_sign.sqlite && \
python norm.py MP_C_.final.rm_sign.sqlite -f && \
python add_dm_view.py MP_C_.final.rm_sign.norm.sqlite && \
python add_xy_view.py MP_C_.final.rm_sign.norm.sqlite -f && \
cp MP_C_.final.rm_sign.norm.sqlite MP_C_.final.dm.sqlite

# python myscript.py MP_C_.final.dm.sqlite

# date +"%T" && python myscript.py MP_C_.final.dm.sqlite # REM | tee MP_C_.final.dm.result.txt && date +"%T"

# SQLite to Excel
# python sqlite2xls.py C_1.sqlite -f
# python sqlite2xls.py C_2.sqlite -f
# python sqlite2xls.py C_3.sqlite -f
# python sqlite2xls.py C_all.sqlite -f

# python sqlite2xls.py MP_1.sqlite -f
# python sqlite2xls.py MP_2.sqlite -f
# python sqlite2xls.py MP_3.sqlite -f
# python sqlite2xls.py MP_all.sqlite -f

# SQLite to C_SV
# python sqlite2csv.py C_1.sqlite -f
# python sqlite2csv.py C_2.sqlite -f
# python sqlite2csv.py C_3.sqlite -f
# python sqlite2csv.py C_all.sqlite -f

# python sqlite2csv.py MP_1.sqlite -f
# python sqlite2csv.py MP_2.sqlite -f
# python sqlite2csv.py MP_3.sqlite -f
# python sqlite2csv.py MP_all.sqlite -f
