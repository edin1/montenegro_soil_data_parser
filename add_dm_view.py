import os
import sqlite3
import helpers as h

def fix(cli_parser, args, options):
    dbconn = sqlite3.connect(args[0],
                             detect_types=sqlite3.PARSE_DECLTYPES,
                             check_same_thread=False)
    #dbconn.text_factory = str
    cursor = dbconn.cursor()
    table = options.table
    types = [i[2] for i in
             cursor.execute("PRAGMA table_info(%s)"%table).fetchall()]
    columns = [i[1] for i in
               cursor.execute("PRAGMA table_info(%s)"%table).fetchall()]

    columns_view = []#["my_row_id"]
    for i, c in enumerate(columns):
        if types[i] in ["DECIMAL", "HUMUS_CHARACTER", "STABILITY"]:
            if c.endswith("_min"): # range/depth columns
                if c.endswith("_depth_min") or c.endswith("depth2_min"):
                    # skip columns that are duplicates
                    # of some other column(s)
                    continue
                columns_view.append(c)
                c_short = c[:-len("_min")]
                columns_view.append("%s - %s as %s_length"%(c_short + "_max", c, c_short))
            else:
                if any([c.endswith(ending)
                        for ending in ["_max", "clay_value", "T_S_value"]]):
                        continue
                # else:
                columns_view.append(c)
    cursor.execute("DROP VIEW IF EXISTS dm_view")
    cmd = """CREATE VIEW dm_view AS
             SELECT %s
             FROM properties;"""%", ".join(columns_view)
    cursor.execute(cmd)
    dbconn.commit()
    dbconn.close()

if __name__ == "__main__":
    h.main(handler=fix)