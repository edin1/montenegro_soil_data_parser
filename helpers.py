# -*- coding: utf-8 -*-
import sys
import subprocess
import os
import platform
from PIL import Image
import shutil
import os
from optparse import OptionParser
import sqlite3
import random
from pprint import pprint
import difflib

BIG_NUMBER = 10000000

SECTIONS = [
    "Višegrad 3",
    "Gacko 2",
    "Žabljak 1",
    "Žabljak 2",
    "Pljevlja 1",
    "Gacko 3",
    "Gacko 4",
    "Žabljak 3",
    "Žabljak 4",
    "Pljevlja 3",
    "Pljevlja 4",
    "Sjenica 3",
    "Trebinje 1",
    "Trebinje 2",
    "Nikšić 1",
    "Nikšić 2",
    "Kolašin 1",
    "Kolašin 2",
    "Peć 1",
    "Peć 2",
    "Trebinje 3",
    "Trebinje 4",
    "Nikšić 3",
    "Nikšić 4",
    "Kolašin 3",
    "Kolašin 4",
    "Peć 3",
    "Kotor 1",
    "Kotor 2",
    "Cetinje 1",
    "Cetinje 2",
    "Skadar 1",
    "Cetinje 3",
    "Cetinje 4",
    "Skadar 3",
    "Ulcinj-Lješ",
]


DATASET1_PROFILE_SUFFIX_SECTION_TO_SKIP = [
]

DATASET1_PROFILE_SUFFIX_TO_SKIP = [
]

DATASET1_PROFILES_DUPLICATE = [
]


DATASET2_PROFILE_SUFFIX_SECTION_TO_SKIP = [
    (15, 'p', 'trebinje4'),
    (18, 'm', 'gacko4'),
    (23, 's', 'gacko4'),
]

DATASET2_PROFILE_SUFFIX_TO_SKIP = [
]

DATASET2_PROFILES_DUPLICATE = [
]


DATASET3_PROFILE_SUFFIX_SECTION_TO_SKIP = [
    (54, None, 'pljevlja1'),
    (55, None, 'pljevlja1'),
    (56, None, 'pljevlja1'),
    (58, None, 'pljevlja1'),
    (47, 'p', 'trebinje2'),
]

DATASET3_PROFILE_SUFFIX_TO_SKIP = [
]

DATASET3_PROFILES_DUPLICATE = [
]


DATASET123_PROFILE_SUFFIX_SECTION_TO_SKIP = [
    (18, 'm', 'gacko4'), # Two very close profiles, unable to discern between them
    (23, 's', 'gacko4'), # Two very close profiles, unable to discern between them
    (47, 'p', 'trebinje2'), # Two very close profiles, unable to discern between them
] + DATASET1_PROFILE_SUFFIX_SECTION_TO_SKIP + DATASET2_PROFILE_SUFFIX_SECTION_TO_SKIP + DATASET3_PROFILE_SUFFIX_SECTION_TO_SKIP

DATASET123_PROFILE_SUFFIX_TO_SKIP = [
]

DATASET123_PROFILES_DUPLICATE = [
    # These (kolasin1) are all same profiles, perhaps different measurments only?
    (96, None, 'kolasin1', 6624404.62, 4759651.22),
    (97, None, 'kolasin1', 6624757.73, 4760178.98),
    (98, None, 'kolasin1', 6624438.8, 4760683.96),
    (99, None, 'kolasin1', 6624955.16, 4760499.82),
    (100, None, 'kolasin1', 6625342.44, 4760423.88),
    (101, None, 'kolasin1', 6626048.66, 4760344.15),
    (102, None, 'kolasin1', 6625672.77, 4760268.21),
    (103, None, 'kolasin1', 6625501.91, 4759789.81),
    (104, None, 'kolasin1', 6625353.83, 4759315.19),
    (105, None, 'kolasin1', 6625995.51, 4759444.29),
    (106, None, 'kolasin1', 6625908.17, 4759675.9),
    (107, None, 'kolasin1', 6626591.61, 4760025.21),
    (109, None, 'kolasin1', 6628171.1, 4757185.16),
    (111, None, 'kolasin1', 6628239.44, 4756892.81)
]


DATASET5_PROFILE_SUFFIX_SECTION_TO_SKIP = [
    (101, None, "pljevlja3"),
]

DATASET5_PROFILE_SUFFIX_TO_SKIP = [
    (214, "e"),
    (340, "a"),
    (242, "a"),
    (58, "a"),
    (365, "a"),
    (301, "a"),
    (312, "a"),
    (316, "e"),
    (361, "a"),
    (49, None),
    (233, None),
    (339, None),
    (218, "a"),
    (305, "e"),
    (309, "e"),
    (338, "a"),
    (350, "b"),
    (317, "e"),
    (318, "e"),
    (213, "a"),
    (333, None),
    (320, None),
    (321, None),
    (261, None),
    (306, None),
    (306, "a"),
    (307, None),
    (274, "e"),
    (311, None),
    (404, "a"),
]

DATASET5_PROFILES_DUPLICATE = [
    (393, None, 'zabljak2', 6598624, 4801589.33),
    (373, None, 'zabljak1', 6582005.19, 4817031.13),
]


# Many of the profiles are repeated in DATASET 4
DATASET4_PROFILE_SUFFIX_SECTION_TO_SKIP = [
    (111, None, 'pljevlja3'),
    (483, None, 'ulcinj-ljes'),
] +\
    DATASET5_PROFILE_SUFFIX_SECTION_TO_SKIP
    # DATASET123_PROFILE_SUFFIX_SECTION_TO_SKIP +\

DATASET4_PROFILE_SUFFIX_TO_SKIP = [
]

DATASET4_PROFILES_DUPLICATE = [
    (39, None, 'zabljak4', 6600894.52, 4777974.82),
    (55, 'm', 'kolasin3', 6624525.32, 4724399.95),
    (8, 'b', 'gacko4', 6563401.07, 4781016.39),
    (32, 'b', 'gacko4', 6556930.6, 4777539.82),
    (5, 'g', 'gacko4', 6555530.71, 4778551.79),
    (6, 'g', 'gacko4', 6556543.34, 4778980.56),
    (14, 's', 'gacko4', 6561672.93, 4776791.55),
    (20, 's', 'gacko4', 6554459.91, 4780193.68),
    (2, 'b', 'pec2', 6674745.45, 4761265.35),
    (441, None, 'ulcinj-ljes', 6607619.99, 4648206.31),
] # +\
    # DATASET123_PROFILES_DUPLICATE +\
    # DATASET5_PROFILES_DUPLICATE


r"""
pdfimages -j -jp2 -jbig2 -ccitt 001.pdf a
convert -resize 1664x2339! Hemijske01-241.jpg Hemijske01-241.pbm
set PATH=C:\Program Files\ImageMagick-6.9.0-Q16;C:\Program Files (x86)\Paragon Software\Paragon ExtFS for Windows;C:\Program Files (x86)\PDFtk Server\bin\;C:\Program Files\MiKTeX 2.9\miktex\bin\x64\;D:\Data\programs\texlive\2013\bin\win32;C:\Program Files (x86)\Google\google_appengine\;c:\Program Files\Java\jdk1.7.0_07\bin\;d:\programs\adt-bundle-windows-x86_64-20140702\sdk\platform-tools;d:\programs\adt-bundle-windows-x86_64-20140702\sdk\tools;d:\programs\adt-bundle-windows-x86_64-20140702\sdk\build-tools\21.1.1;d:\programs\adt-bundle-windows-x86_64-20140702\sdk\..\eclipse\plugins\org.apache.ant_1.8.3.v201301120609\bin\;C:\Users\Edin\AppData\Roaming\npm;d:\programs\ffmpeg-20141120-git-4388e78-win64-static\bin;d:\programs\mp3gain-win-full-1_2_5;d:\programs\poppler-0.26.4\bin;C:\Program Files (x86)\GnuWin32\bin

for f in *.pbm; do echo $f && ppm2tiff -c g4 $f "${f%.*}".tiff; done
for %f in (*.pbm) do (echo %~nf && ppm2tiff -c g4 "%~nf.pbm" "%~nf.tiff")
for %f in (*.tiff) do (echo %~nf && tiff2pdf "%~nf.tiff" -o"%~nf.pdf")
pdftk *.pdf cat output H1.pdf

(for %f in (*.pbm) do (ppm2tiff -c g4 "%~nf.pbm" "%~nf.tiff")) && (for %f in (*.tiff) do (tiff2pdf "%~nf.tiff" -o"%~nf.pdf")) && (pdftk *.pdf cat output M3.pdf)

pdfimages -j ../H1.pdf -list

FALI U PDF-U
MORA SE POGLEDATI
U ORIGINALNOJ KNJIZI

"""

def myconvert(e):
    if e is not None:
        return e
    else:
        return ""
    # if isinstance(e, unicode):
    #     return e.encode("utf-8")
    # elif e is None:
    #     return ""
    # else:
    #     return e

def random_name():
    return ''.join(
        random.choice("abcdefghijklmnopqrstuvwxyz") for i in range(16))

def get_columns_rows(sqlitefile, table):
    con = sqlite3.connect(sqlitefile)
    cur = con.cursor()

    types = [i[2] for i in cur.execute("PRAGMA table_info(%s)"%table).fetchall()]
    columns = [i[1] for i in cur.execute("PRAGMA table_info(%s)"%table).fetchall()]

    sql = "select %s from %s"%(", ".join(columns), table)

    rows = cur.execute(sql)
    return columns, types, rows

def get_columns_rows_for_sql(sqlitefile, sql):
    # Check whether sql is actually a filename
    if os.path.isfile(sql):
        with open(sql) as sqlfile:
            sql = sqlfile.read()

    if isinstance(sqlitefile, sqlite3.Connection):
        close_connection = False
        con = sqlitefile
    else:
        close_connection = True
        con = sqlite3.connect(sqlitefile,
                             detect_types=sqlite3.PARSE_DECLTYPES,
                             check_same_thread=False,
                         isolation_level=None)
    if not sql.lower().strip().startswith("select"):
        sql = " ".join(sql.split())
        try:
            preamble, sql = sql.split(" select ", 1)
        except ValueError:
            raise ValueError("You have to have a select statement in your SQL code")
        sql = "select " + sql
        # Execute the preamble
        con.executescript(preamble)
    tmp_view_name = random_name()

    con.execute("BEGIN")
    cmd = "CREATE VIEW %s AS "%tmp_view_name + sql
    # print(cmd)
    con.execute(cmd)
    columns = [i[1] for i in
        con.execute("PRAGMA table_info(%s)"%tmp_view_name).fetchall()]
    types = [i[2] for i in
        con.execute("PRAGMA table_info(%s)"%tmp_view_name).fetchall()]

    rows = con.execute("select * from " + tmp_view_name).fetchall()

    # Cleanup
    # cur.execute("DROP VIEW IF EXISTS " + tmp_view_name)
    con.execute("ROLLBACK")
    if close_connection:
        con.close()
    return columns, types, rows

def main(ext=None, handler=None, parse_args=True):
    cli_parser = create_cli_parser()
    if parse_args:
        cli_parser, args, options = parse_cli_parser(cli_parser, ext)
        handler(cli_parser, args, options)
    else:
        return handler(cli_parser)

def create_cli_parser():
    cli_parser = OptionParser(
        usage='usage: %prog [options] <input.sqlite>'
        )
    cli_parser.add_option("-f", "--force", action="store_true",
                      dest="force", default=False,
                      help="Overwrite output file if it already exists.")
    cli_parser.add_option("-t", "--table",
                      dest="table",
                      help="Name of the table to be exported.")
    cli_parser.add_option("-o", "--outfile",
                      dest="outfile",
                      help="Name of the output file.")
    cli_parser.add_option("-e", "--ext",
                      dest="ext",
                      help="Name of the extension for the output file.")
    cli_parser.add_option("-c", "--columns",
                      dest="columns",
                      help="Name of columns to be selected.")
    cli_parser.add_option("-s", "--sql",
                      dest="sql",
                      help="SQL command to use for selecting data.")
    cli_parser.add_option("--handler",
                      dest="handler",
                      help="Name of the handler function to be executed.")
    return cli_parser

def parse_cli_parser(cli_parser, ext):
    (options, args) = cli_parser.parse_args()

    if options.ext:
        ext = options.ext

    if len(args) < 1:
        cli_parser.error("You have to supply at least 1 argument")
    else:
        file_ = args[0]#.decode("utf-8")
        if not options.table:
            options.table = "properties"
            # cli_parser.error("You have to supply a valid SQLite table name "
            #                  "with --table (or -t)")

        if options.outfile:
            outfile = options.outfile
        elif ext is None:
            print("Warning: no file ending supplied, "
                  "there will be no output file.")
            outfile = None
        else:
            outfile = os.path.splitext(file_)[0] + ext

        if outfile is not None and os.path.exists(outfile):
            if options.force:
                os.remove(outfile)
            else:
                msg = "Warning: output file %s already exists. Use --force (or -f) " \
                  "to overwrite this file."%outfile
                print(msg)
                # cli_parser.error(msg)
        options.outfile = outfile
        return (cli_parser, args, options)

def shellexecute(filepath):
    if sys.platform.startswith('darwin'):
        subprocess.call(('open', filepath))
    elif os.name == 'nt':
        os.startfile(filepath)
    elif os.name == 'posix':
        subprocess.call(('xdg-open', filepath))

def show_excel(book, page_begin, page_end, entry_person):
    fexcel = ", ".join([book, "str %s-%s"%(page_begin, page_end), entry_person])
    fexcel = os.path.join(book, fexcel + ".xls")
    if not os.path.exists(fexcel):
        fexcel = fexcel[:-len(".xls")] + ".xlsx"
    shellexecute(fexcel)

def show_pdf_page(pdf, page):
    # img = "show_pdf_page_images-0000.pbm" #xpdf name
    img = "show_pdf_page_images-000.pbm" #xpdf name
    if os.path.exists(img):
        os.remove(img)
    args = "pdfimages -j -f %d -l %d"%(page, page)
    p = subprocess.Popen(args.split() + [pdf, "show_pdf_page_images"])
    p.communicate()[0]
    if p.returncode != 0:
        print(p.returncode)
        sys.exit()
    else:
        shellexecute(img)

def show_id(file_, my_row_id):
    dbconn = sqlite3.connect(file_,
                             detect_types=sqlite3.PARSE_DECLTYPES,
                             check_same_thread=False)
    cursor = dbconn.cursor()
    sql = """
select book, page_begin, page_end, entry_person, page, page_row, excel_row
from properties
where my_row_id = %s
"""%my_row_id

    book, page_begin, page_end, entry_person, page, page_row, \
        excel_row = cursor.execute(sql).fetchone()
    print(book, page, page_row)
    print("Excel row: ", excel_row)
    show_book_page(book, page)
    show_excel(book, page_begin, page_end, entry_person)

def show_my_profile_id(file_, my_profile_id):
    dbconn = sqlite3.connect(file_,
                             detect_types=sqlite3.PARSE_DECLTYPES,
                             check_same_thread=False)
    cursor = dbconn.cursor()

    columns = [i[1] for i in cursor.execute("PRAGMA table_info(properties)").fetchall()]
    prefix = get_columns_prefix(columns)
    if prefix == "MP_":
        other_prefix = "C_"
    elif prefix == "C_":
        other_prefix = "MP_"
    elif prefix != "":
        raise Exception("Unrecognized prefix")

    try:
        my_profile_id = int(my_profile_id)
    except ValueError:
        raise Exception("<my_profile_id> must be a valid ID "
            "(i.e. integer number)")

    if my_profile_id > BIG_NUMBER: # Switch prefixes
        prefix, other_prefix = other_prefix, prefix
        my_profile_id -= BIG_NUMBER
    sql = """
select {prefix}book, {prefix}page_begin, {prefix}page_end, {prefix}entry_person, {prefix}page, {prefix}page_row, {prefix}excel_row
from properties
where {prefix}my_profile_id = {my_profile_id}
order by {prefix}excel_row desc
""".format(prefix=prefix, my_profile_id=my_profile_id)
    book, page_begin, page_end, entry_person, page, page_row, \
        excel_row = cursor.execute(sql).fetchone()
    print(book, page, page_row)
    print("Excel row: ", excel_row)
    show_book_page(book, page)
    show_excel(book, page_begin, page_end, entry_person)


def show_book_page(book, page):
    shellexecute(os.path.join(book, "images", "%s.tiff"%page))

def merge_images(outfile, *imagesf):
    width = 0
    height = 0
    mode = None
    images = []
    for imgf in imagesf:
        img = Image.open(imgf)
        images.append(img)
        width += img.size[0]
        height = max(height, img.size[1])
        if mode is None:
            mode = img.mode
        elif mode != img.mode:
            raise Exception("Image %s has a different mode %s!"%(imgf, img.mode))
    im_new = Image.new(mode, (width, height))
    width = 0
    for img in images:
        im_new.paste(img, (width, 0, width + img.size[0], height))
        width += img.size[0]
    im_new.save(outfile)

def merge_images_directory(dirname):
    files = os.listdir(dirname)
    files.sort()
    for i in range(0, len(files), 2):
        fname1 = os.path.join(dirname, files[i])
        fname2 = os.path.join(dirname, files[i+1])
        # outfname = "%03d0-%s-%s.png"%(i//2+1, files[i], files[i+1])
        outfname = "%03d.pbm"%(i//2+1)
        print(outfname)
        merge_images(os.path.join(dirname, outfname), fname1, fname2)

def rename_images_directory(dirname):
    files = os.listdir(dirname)
    files.sort()
    os.chdir(dirname)
    for i in range(len(files)):
        shutil.copyfile(files[i], "%03d.pbm"%(i+1))

def create_db(fname, table, columns, rows):
    if isinstance(fname, sqlite3.Connection):
        con = fname
    else:
        con = sqlite3.connect(fname)
    con.execute("DROP TABLE IF EXISTS %s"%table)
    _columns = "\n,".join("%s %s"%(col[0], col[1]) for col in columns)
    cmd_create = """
        create table IF NOT EXISTS %s(
        %s
        );

        """%(table, _columns)
    con.execute(cmd_create)
    _columns = ", ".join(col[0] for col in columns)
    cmd_insert ="""
        INSERT INTO %s
        (%s)
        VALUES
        (%s)"""%(table, _columns, ", ".join(["?"]*len(columns)))
    for row in rows:
        con.execute(cmd_insert, row)
    con.commit()

def get_index(columns):
    prefix = get_columns_prefix(columns)
    def _f(text):
        return columns.index(prefix + text)
    return _f

def get_columns_prefix(columns):
    if columns[0].startswith("MP_"):
        prefix = "MP_"
    elif columns[0].startswith("C_"):
        prefix = "C_"
    else:
        prefix = ""
    return prefix


def mypath(f):
    if platform.system() == "Linux":
        f = f.replace("c:", "/media/Evo850-1TB-1")
        f = f.replace("C:", "/media/Evo850-1TB-1")
        f = f.replace("\\", "/")
    return f

def difflists(list1, list2):
    t1 = [str(i) for i in list1]
    t2 = [str(i) for i in list2]
    return "\n".join(difflib.unified_diff(t1, t2))

def extract_dataset_number(filename):
    text = filename.split(".")[0]
    text_new = ""
    for c in text:
        if c.isdigit():
            text_new += c
        else:
            text_new += " "
    numbers = text_new.split()
    assert len(numbers) == 1
    return int(numbers[0])
