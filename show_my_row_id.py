import sqlite3
import helpers as h

def fix(cli_parser, args, options):
    cli_parser.usage += " <my_row_id>"
    if len(args) != 2:
        cli_parser.error("You have to supply exactly 2 arguments")
    # else:
    my_row_id = args[1]
    try:
        int(my_row_id)
    except ValueError:
        cli_parser.error("<my_row_id> must be a valid ID "
            "(i.e. integer number)")

    h.show_id(args[0], my_row_id)

if __name__ == "__main__":
    h.main(handler=fix)