# About #

This repository contains the code written for
parsing/processing Montenegro's soil data entered in Excel files.
The repository does not contain the actual data,
and its purpose is only to showcase the code.
The code is of pre-alpha quality as it was created and used
by only one person.

The file parse\_all.bat is the "main" script that was
used to invoke other scripts. It basically gives an overview
of the whole data processing process.