import os
import sqlite3
import helpers as h

def fix(cli_parser, args, options):

    if options.sql is None:
        cli_parser.error("You have to supply an SQL expression")

    columns, types, rows = h.get_columns_rows_for_sql(args[0], options.sql)

    h.create_db(options.outfile, options.table,
                list(zip(columns, types)), rows)

if __name__ == "__main__":
    h.main(handler=fix)