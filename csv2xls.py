import sys
import os
import csv
import xlwt

w = xlwt.Workbook()
ws = w.add_sheet('Data')


def csv2xls(fcsv, fxls):
    w = xlwt.Workbook()
    ws = w.add_sheet('Data')
    rows = []
    with open(fcsv) as f:
        reader = csv.reader(f)
        for r, row in enumerate(reader):
            for c, cell in enumerate(row):
                ws.write(r, c, row[c])
    w.save(fxls)


def main():
    fcsv = sys.argv[1]
    try:
        fxls = sys.argv[2]
    except IndexError:
        fxls = os.path.splitext(fcsv)[0] + '.xls'
    csv2xls(fcsv, fxls)


if __name__ == '__main__':
    main()
