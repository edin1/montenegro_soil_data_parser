import sqlite3
import helpers as h

def fix(cli_parser, args, options):
    cli_parser.usage = "usage: %prog [options] <book> <page>"
    if len(args) != 2:
        cli_parser.error("You have to supply exactly 2 arguments")
    # else:
    book, page = args
    try:
        page = "%03d"%int(page)
    except ValueError:
        cli_parser.error("<page> has to be a valid page number")
    book = book.upper()
    books = ["C_1", "C_2", "C_3", "MP_1", "MP_2", "MP_3"]
    if book not in books:
        cli_parser.error("<book> has to be a valid book name, i.e. "
                         "%s"%", ".join(books))
    h.show_book_page(book, page)

if __name__ == "__main__":
    h.main(handler=fix)