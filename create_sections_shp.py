from collections import defaultdict
import shapefile

X_DELTA = 0.24997
X_MIN = 18.3317
X_MAX = X_MIN + X_DELTA*8

Y_DELTA = 0.24999
# Y_MIN = 41.7506
Y_MIN = 42.0006
Y_MAX = Y_MIN + Y_DELTA*5

SECTIONS = defaultdict(lambda: None, {
    2: "kotor1",
    3: "trebinje3",
    4: "trebinje1",
    5: "gacko3",
    8: "kotor2",
    9: "trebinje4",
    10: "trebinje2",
    11: "gacko4",
    12: "gacko2",
    13: "cetinje3",
    14: "cetinje1",
    15: "niksic3",
    16: "niksic1",
    17: "zabljak3",
    18: "zabljak1",
    19: "cetinje3",
    20: "cetinje2",
    21: "niksic4",
    22: "niksic2",
    23: "zabljak4",
    24: "zabljak2",
    25: "skadar3",
    26: "skadar1",
    27: "kolasin3",
    28: "kolasin1",
    29: "pljevlja3",
    30: "pljevlja1",
    33: "kolasin4",
    34: "kolasin2",
    35: "pljevlja4",
    39: "pec3",
    40: "pec1",
    41: "sjenica3",
    46: "pec2",
})

w = shapefile.Writer(shapefile.POLYGON)
w.field("id", "N")
w.field("name", "C")

i = 1
parts = []
records = []
x = X_MIN
while x <= X_MAX:
    y = Y_MIN
    while y <= Y_MAX:
        w.record(i, SECTIONS[i])
        polygon = [
            [x, y],
            [x, y + Y_DELTA],
            [x + X_DELTA, y + Y_DELTA],
            [x + X_DELTA, y],
            [x, y],
        ]
        w.poly(parts=[polygon])
        # parts.append(polygon)
        # records.append(str(i))
        y += Y_DELTA
        i += 1
    x += X_DELTA


# w.poly(parts=parts)
# w.record(*records)

w.save('./sections.shp')

r = shapefile.Reader('/home/edin/grid.php')