import parse_chemical

COLUMNS = parse_chemical.OD([
    ("location", "TEXT"),
    ("profile", "PROFILE"),
    ("depth", "RANGE"),
    ("problem", "TEXT"),
    ("section2", "SECTION"),
    ("square", "SQUARE"),
    ("x", "COORDINATE"),
    ("y", "COORDINATE"),
    ("type_1_full_0_empty", "ZERO_OR_ONE"),
])


if __name__ == "__main__":
    parse_chemical.COLUMNS = COLUMNS
    parse_chemical.main()
