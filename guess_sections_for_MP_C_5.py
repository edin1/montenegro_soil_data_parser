import sys
import os
from pprint import pprint
import sqlite3
from collections import defaultdict

import helpers as h

def handler(cli_parser, args, options):
    columns, types, rows = h.get_columns_rows(args[0], options.table)
    ci = h.get_index(columns)
    
    columns_coordinates, types_coordinates, rows_coordinates = h.get_columns_rows(args[1], options.table)
    ci_coordinates = h.get_index(columns_coordinates)


    con = sqlite3.connect(args[1])

                      #profile             section     x, y
    my_profile_ids_processed = {}
    rows_new = []
    for _row in rows:
        _row = list(_row)
        my_profile_id = _row[ci("my_profile_id")]
        if my_profile_id in my_profile_ids_processed:
            _row[ci("section")] = _row[ci("section2")] = my_profile_ids_processed[my_profile_id]['section']
            _row[ci("x2")] = my_profile_ids_processed[my_profile_id]['x']
            _row[ci("y2")] = my_profile_ids_processed[my_profile_id]['y']
            rows_new.append(_row)
            continue
        profile_number = _row[ci("profile_number")]
        profile_suffix = _row[ci("profile_suffix")]
        section2 = _row[ci("section2")]
        square = _row[ci("square")]
        x2 = _row[ci("x2")]
        y2 = _row[ci("y2")]
        x2y2 = (x2, y2)

        # Rows that could not be found on the map
        if (profile_number == 21 and _row[ci("location")].find("šarac") != 1):
            rows_new.append(_row)
            continue

        if None not in x2y2:
            # When x2 and y2 are specified, no need for anything else
            cmd = '''select section from properties where
                     x="%s" and
                     y="%s"'''%x2y2
            sections = [_i[0] for _i in con.execute(cmd).fetchall()]
            assert len(sections) == 1
            section = sections[0]
            x = x2
            y = y2
        elif section2 is not None:
            section = section2
            cmd = "select x, y from properties " \
                  "where profile_number=%d "%profile_number
            if profile_suffix is None:
                cmd += 'and profile_suffix is null'
            else:
                cmd += 'and profile_suffix="%s"'%profile_suffix
            cmd += ' and section="%s"'%section
            results = con.execute(cmd).fetchall()
            if len(results) == 0:
                print(profile_number, profile_suffix, section)
                msg = "No coordinate matching this profile+section."
                raise Exception(msg)
            elif len(results) > 1:
                if (profile_number, profile_suffix, section) in h.DATASET5_PROFILE_SUFFIX_SECTION_TO_SKIP:
                    my_profile_ids_processed[my_profile_id] = {
                        "section": section,
                        "x": None,
                        "y": None,
                    }
                    rows_new.append(_row)
                    continue
                # else:
                print(profile_number, profile_suffix, section)
                print(results)
                msg = "Same profile number for multiple points in the same section. "
                msg += "Try specifying x and y."
                raise Exception(msg)
            else:
                x = results[0][0]
                y = results[0][1]
        else: # x2, y2, section2 are None
            if (profile_number, profile_suffix) in h.DATASET5_PROFILE_SUFFIX_TO_SKIP:
                rows_new.append(_row)
                continue
            else:
                print(profile_number, profile_suffix)
                cmd = "select section, x, y from properties " \
              "where profile_number=%d "%profile_number
                if profile_suffix is None:
                    cmd += 'and profile_suffix is null'
                else:
                    cmd += 'and profile_suffix="%s"'%profile_suffix
                # print(" ".join(cmd.split()))
                print(con.execute(cmd).fetchall())
                msg = "You have to specify the section of this profile."
                raise Exception(msg)
        xy = (x, y)
        rows_new.append(_row)
        my_profile_ids_processed[my_profile_id] = {
            "section": section,
            "x": x,
            "y": y,
        }
    # h.pprint(rows_new)
    print("Creating dbs...")
    h.create_db(options.outfile, "properties",
        list(zip(columns, types)), rows_new)
    # h.create_db(args[0].rsplit(".")[0] + ".section_null.sqlite", "properties",
    #     list(zip(columns, types)), rows_unknown)

if __name__ == "__main__":
    h.main(ext=".section_guessed.sqlite",
           handler=handler)