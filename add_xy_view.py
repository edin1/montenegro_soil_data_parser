import os
import sqlite3
import helpers as h

def fix(cli_parser, args, options):
    con = sqlite3.connect(args[0],
                             detect_types=sqlite3.PARSE_DECLTYPES,
                             check_same_thread=False)
    types = [i[2] for i in
        con.execute("PRAGMA table_info(%s)"%options.table).fetchall()]
    columns = [i[1] for i in
        con.execute("PRAGMA table_info(%s)"%options.table).fetchall()]

    ci = h.get_index(columns)
    #dbconn.text_factory = str
    con.execute("DROP VIEW IF EXISTS xy_view")
    cmd = """
        CREATE VIEW xy_view AS
        SELECT *, (%s || " " || ifnull(%s, "") || ifnull(%s, "")
            || " " || ifnull(%s, "") ||  "-" || ifnull(%s, "")) name FROM properties
        WHERE x IS NOT NULL and y IS NOT NULL;"""%(columns[ci("section")],
                                 columns[ci("profile_number")],
                                 columns[ci("profile_suffix")],
                                 columns[ci("depth_min")],
                                 columns[ci("depth_max")],
                                )
    con.execute(cmd)
    con.commit()
    con.close()

if __name__ == "__main__":
    h.main(handler=fix)