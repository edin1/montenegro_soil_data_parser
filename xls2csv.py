import csv

import xlrd

import helpers as h


def get_string(sheet, row, col):
    try:
        cell = sheet.cell(row, col)
    except IndexError:
        return None
        # print(sheet.cell(row, col-1))
        # print(dir(sheet), row, col)
        # raise
    v = cell.value

    if cell.ctype == 3:
        print("%s%d"%(chr(ord("A") + col), row+1), repr(v))
        raise Exception("DATE type found")

    v = str(v).strip()

    if cell.ctype == 2 and v.endswith(".0"): # remove the decimal zero added by sqlite
        v = v[:-len(".0")]

    if col not in [1, 20]:
        v = v.lower()
    if v == "":
        return None
    v = " ".join(v.split())
    v = ", ".join([word.strip() for word in v.split(",")])
    return v


def get_row(sheet, row):
    return [get_string(sheet, row, col) for col in range(sheet.ncols)]


def xls2csv(cli_parser, args, options):
    wb = xlrd.open_workbook(args[0])
    sheet = wb.sheets()[0]

    rows_new = []
    for i in range(sheet.nrows):
        rows_new.append(get_row(sheet, i))

    with open(options.outfile, 'w', encoding='utf8') as csvfile:
        csvwriter = csv.writer(csvfile, lineterminator='\n')
        for row in rows_new:
            row_new = [h.myconvert(e) for e in row]
            csvwriter.writerow(row_new)

if __name__ == "__main__":
    h.main(ext=".csv", handler=xls2csv)