import os
import sqlite3
import decimal
from pprint import pprint
import helpers as h

def str2float(s):
    if isinstance(s, str):
        s = s.replace(",", ".")
    return float(s)

def adapt_decimal(d):
    return str(d)

def convert_decimal(s):
    if isinstance(s, bytes):
        s = s.decode("utf-8")
    return decimal.Decimal(s)

# Register the adapter
sqlite3.register_adapter(decimal.Decimal, adapt_decimal)

# Register the converter
sqlite3.register_converter("decimal", convert_decimal)

def fix(cli_parser, args, options):
    dbconn = sqlite3.connect(args[0],
                             # detect_types=sqlite3.PARSE_DECLTYPES,
                             check_same_thread=False)
    #dbconn.text_factory = str
    cursor = dbconn.cursor()
    table = options.table
    rows = cursor.execute("select * from %s"%table).fetchall()
    print(len(rows))
    types = [i[2] for i in
             cursor.execute("PRAGMA table_info(%s)"%table).fetchall()]
    columns = [i[1] for i in
               cursor.execute("PRAGMA table_info(%s)"%table).fetchall()]

    columns_decimal = []
    for i, c in enumerate(columns):
        if c in ("x", "y", "X", "Y") or c.lower().find("depth") != -1:
            continue
        if types[i] == "DECIMAL":
            columns_decimal.append(c)

    cmd = "select %s"%", ".join(
        ["avg(%s)"%c for c in columns_decimal]
    )
    cmd += "\nfrom %s"%table
    print(cmd)
    columns_decimal_avgs = cursor.execute(cmd).fetchone()

    cmd = "select %s"%", ".join(
        ["max(abs(%s))"%c for c in columns_decimal]
    )
    cmd += "\nfrom %s"%table
    print(cmd)
    columns_decimal_maxs = cursor.execute(cmd).fetchone()

    # print(columns_decimal, len(columns_decimal))
    # print(columns_decimal_maxs, len(columns_decimal_maxs))
    pprint(list(zip(columns_decimal,
                    columns_decimal_avgs,
                    columns_decimal_maxs)))

    # cmd = "select %s " \
    #       "from properties " \
    #       "order by my_row_id"%(", ".join(columns))

    dbconn2 = sqlite3.connect(options.outfile,
                             #detect_types=sqlite3.PARSE_DECLTYPES,
                             check_same_thread=False)
    cursor2 = dbconn2.cursor()

    cursor2.execute("DROP TABLE IF EXISTS %s"%table)

    cmd = "CREATE TABLE IF NOT EXISTS %s("%table
    cmd += """
            %s
    )
    """%(",\n".join(["%s %s"%(c, t) for c, t in zip(columns, types)]))
    # print(cmd)
    cursor2.execute(cmd)

    cmd = "INSERT INTO %s"%table
    cmd += """
                (%s)
                VALUES
                (%s)"""%(", ".join(columns), ", ".join(["?"]*len(columns)))

    counter = 0
    for i, row in enumerate(rows):
        counter += 1
        new_row = list(row)
        for i, c in enumerate(columns):
            if c in columns_decimal and new_row[i] is not None:
                index = columns_decimal.index(c)
                avg = str2float(columns_decimal_avgs[index])
                numerator = str2float(new_row[i])
                denominator = str2float(columns_decimal_maxs[index])
                new_row[i] = (numerator - avg) / (denominator - avg)
        cursor2.execute(cmd, new_row)
    print(counter)
    cursor2.execute("DROP TABLE IF EXISTS columns_decimal_avgs_maxs")

    cmd = """CREATE TABLE IF NOT EXISTS columns_decimal_avgs_maxs(
            %s
    )
    """%(",\n".join(["%s %s"%(c, t) for
                     c, t in zip(columns_decimal,
                                 ["DECIMAL"]*len(columns_decimal))]))
    cursor2.execute(cmd)
    cmd = """INSERT INTO columns_decimal_avgs_maxs
                (%s)
                VALUES
                (%s)"""%(", ".join(columns_decimal),
                         ", ".join(["?"]*len(columns_decimal)))
    cursor2.execute(cmd, columns_decimal_avgs)
    cursor2.execute(cmd, columns_decimal_maxs)
    dbconn2.commit()
    dbconn2.close()
    dbconn.close()


if __name__ == "__main__":
    h.main(ext=".%s.sqlite"%os.path.splitext(os.path.basename(__file__))[0],
           handler=fix)