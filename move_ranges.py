import os
import sys
import csv
import re
import xlrd
import helpers as h
from parse_chemical import get_RANGE, InvalidRangeException

def get_row(sheet, row):
    values = []
    for col in range(sheet.ncols):
        values.append(get_string(sheet, row, col))
    return values

def parse_row(sheet, row):
    pass

def get_string(sheet, row, col):
    try:
        cell = sheet.cell(row, col)
    except IndexError:
        return None
        # print(sheet.cell(row, col-1))
        # print(dir(sheet), row, col)
        # raise
    return cell.value

def main():
    fname = sys.argv[1]
    fname_abs = os.path.abspath(fname)
    fname_base = os.path.basename(fname_abs)

    fname_new, ext = os.path.splitext(fname_abs)
    fname_new += "_new.csv"

    wb = xlrd.open_workbook(fname_abs)
    sheet = wb.sheets()[0]

    # print(dir(sheet))

    rows = []
    rows_for_profile_previous = []
    profile_previous = None
    for row in range(sheet.nrows):
        values = get_row(sheet, row)
        try:
            range_ = get_RANGE(sheet, row, 2)
        except Exception:
            print("Invalid range:")
            rows.append(values)
            continue
        # else:
        values.insert(0, None)
        rows.append(values)

    with open(fname_new, 'w', encoding='utf8') as csvfile:
        csvwriter = csv.writer(csvfile, lineterminator='\n')
        for row in rows:
            row_new = [h.myconvert(e) for e in row]
            csvwriter.writerow(row_new)


if __name__ == "__main__":
    main()