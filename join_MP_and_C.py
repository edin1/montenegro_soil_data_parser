import sys
import os
import sqlite3
import difflib
import itertools
from pprint import pprint

import helpers as h

IGNORE_DIFFERING_NUMBER_OF_HORIZONTS = True

PROBLEMATIC_ACCEPTABLE_PAIRS = [
    [('C_1', '011', 5), ('MP_1', '011', 7)],
    [('C_1', '011', 24), ('MP_1', '011', 26)],
    [('C_1', '084', 29), ('MP_1', '085', 29)],
    [('C_1', '085', 10), ('MP_1', '086', 10)],
    [('C_1', '112', 15), ('MP_1', '116', 15)],
    [('C_1', '128', 28), ('MP_1', '133', 1)],
    [('C_1', '135', 17), ('MP_1', '139', 18)],
    [('C_1', '153', 3), ('MP_1', '155', 21)],
    [('C_1', '159', 4), ('MP_1', '160', 11)],
    [('C_2', '058', 14), ('MP_2', '032', 25)],
    [('C_2', '059', 25), ('MP_2', '034', 8)],
    [('C_2', '124', 1), ('MP_2', '084', 21)],
]

PROBLEMATIC_UNACCEPTABLE_PAIRS = [
    [('C_1', '017', 29), ('MP_1', '018', 4)],
    [('C_1', '148', 2), ('MP_1', '152', 26)],
    [('C_2', '155', 15), ('MP_3', '022', 1)],
    [('C_2', '155', 16), ('MP_3', '022', 1)],
    [('C_3', '042', 5), ('MP_3', '063', 1)],
    [('C_3', '098', 1), ('MP_3', '112', 1)],
    [('C_3', '100', 9), ('MP_3', '110', 1)],
    [('C_3', '119', 18), ('MP_3', '131', 17)],
    [('C_2', '156', 11), ('MP_4', '195', 27)],
    [('C_3', '031', 5), ('MP_4', '217', 17)],
    [('MP_4', '217', 26), ('C_3', '031', 23)],
]

def concat(lists):
    res = []
    for sublist in lists:
        res.extend(sublist)
    return res

def flatten(list_):
    rows = list()
    index = 0
    indexes = []
    for i, e in enumerate(list_):
        indexes.append(index)
        index += len(e)
        rows.extend(e)
    return indexes, rows

def compare(list1, list2):
    """Returns:
    - the number of rows that are the same in both lists
    - the position of the first element in list1
     that doesn't match the respective element from list2
    """
    c = 0
    if list1[0] != list2[0]:
        return 0, 0
    i1_result = None
    for i1, e1 in enumerate(list1):
        try:
            i2 = list2.index(e1)
            list2 = list2[i2+1:]
            c += 1
        except ValueError:
            if i1_result is None:
                i1_result = i1
            pass
        if not list2:
            break
    if i1_result is None:
        i1_result = len(list1) - 1
    return c, i1_result

def get_problematic_profile(rows, i, n):
    i_min = 0
    j = i
    while i_min <= n:
        row = rows[j]
        i_min += len(row)
        j += 1
    return j - 1


def handler_thorough(cli_parser, args, options):
    "Join MP and C dbs on profile and depth thoroughly (slow)"
    table = options.table
    columns1, keys1, locations1,\
        profile_ids1, rows1 = get_profile_rows(args[0], table)
    # h.pprint(profile_ids)
    columns2, keys2, locations2,\
        profile_ids2, rows2 = get_profile_rows(args[1], table)
    # indexes1, rows1 = flatten(rows1_)
    # indexes2, rows2 = flatten(rows2_)

    # a1 = [1,2,3,4,5]
    # a2 = [1,2,3,5,6]
    # print(count_equal_elements(a1, a2))
    # sys.exit()
    M = 5
    N = 10
    count1 = set()

    # 1942 profiles in C1
    # 1751 profiles in MP1
    # 2876-1751 = 1125 profiles in MP2
    try:
        with open("join_start_profiles.txt") as f:
            i1_start, i2_start = f.read().split()
            i1_start = max(int(i1_start) - 2, 0)
            i2_start = max(int(i2_start) - 2, 0)
    except (FileNotFoundError, ValueError):
        i1_start = 0
        i2_start = 0
    for i1 in range(i1_start, len(rows1)-M+1):
        # if i1 > 100:
        #     break
        row_block1 = concat(rows1[i1:i1+M])[:N]
        N1_actual = len(row_block1)
        for i2 in range(0, len(rows2)-M+1):
            row_block2 = concat(rows2[i2:i2+M])[:N]
            N2_actual = len(row_block2)
            # if row_block1 == row_block2:
            #     i1_start = i1
            #     i2_start = i2
            #     break
            if row_block1[0] == (None,)*len(row_block1[0]):
                break
            t1 = [str(i) for i in row_block1]
            t2 = [str(i) for i in row_block2]
            diff = "\n".join(difflib.unified_diff(t1, t2))
            # h.pprint(t1)
            # h.pprint(t2)
            c1, n1 = compare(t1, t2)
            c2, n2 = compare(t2, t1)
            # print(c)
            diff1_tolerated = int(N1_actual/5)
            diff2_tolerated = int(N2_actual/5)
            def update_count1():
                res = []
                N1_total = 0
                N2_total = 0
                for j in range(0, M):
                    res.append((profile_ids1[i1+j], profile_ids2[i2+j]))
                    N1_total += len(rows1[i1+j])
                    N2_total += len(rows2[i2+j])
                    if N1_total != N2_total:
                        res.pop()
                        break
                    if N1_total >= N:
                        break
                if res:
                    count1.update(res)
                    print(len(count1))
                    print(profile_ids1[i1], N1_actual, c1, diff1_tolerated)
                    print(profile_ids2[i2], N2_actual, c2, diff2_tolerated)

            if c1 == N1_actual and c2 == N2_actual:
                update_count1()
                i1_start = i1
                i2_start = i2
                break
            elif ((N1_actual - c1) < diff1_tolerated) and ((N2_actual - c2) < diff2_tolerated):
                i1_probl = get_problematic_profile(rows1, i1, n1)
                i2_probl = get_problematic_profile(rows2, i2, n1)
                # if keys1[i1_probl] in [
                #     ('C1', '011', 5),
                #     ('C1', '011', 24),
                #     ('C1', '084', 29),
                #     ('C1', '085', 10),
                #     ('C1', '112', 15),
                #     ('C1', '128', 28),
                #     ('C1', '135', 17),
                #     ('C1', '148', 2),
                #     ('C1', '148', 3),
                #     ('C1', '148', 4),
                #     ('C1', '149', 28),
                #     ('C1', '153', 3),
                #     ('C1', '157', 29),
                #
                #     ('C2', '058', 14),
                #     ('C2', '059', 25),
                #     ('C2', '124', 1), # Probably a mistake 35b should be 38b (see MP)
                #     ('C3', '022', 19),
                #     ('C3', '042', 5), # There's no equivalent in MP for this (and following) rows
                # ]:
                #     break
                # if keys2[i2_probl] in [
                #     ('MP1', '160', 11),
                # ]:
                #     break
                keypair = sorted((keys1[i1_probl], keys2[i2_probl]))
                if keypair in \
                        PROBLEMATIC_ACCEPTABLE_PAIRS:
                    update_count1()
                    i1_start = i1
                    i2_start = i2
                    break
                elif keypair in \
                        PROBLEMATIC_UNACCEPTABLE_PAIRS:
                    continue
                #     [
                #     (('C3', '098', 1), ('MP3', '112', 1)),
                #     (('C3', '119', 18), ('MP3', '131', 17)),
                # ]:
                with open("join_start_profiles.txt", "w") as f:
                    f.write("%d %d"%(i1_start, i2_start))
                print(keys1[i1_probl])
                print(keys2[i2_probl])
                print(i1_probl, profile_ids1[i1_probl])
                print(i2_probl, profile_ids2[i2_probl])
                print("Suspicious row")
                h.pprint(t1)
                h.pprint(t2)
                # print(profile_ids1[i1])
                # Find the exact problematic profiles
                h.show_my_profile_id(args[0], profile_ids1[i1_probl])
                # print(profile_ids2[i2])
                h.show_my_profile_id(args[1], profile_ids2[i2_probl])
                print(diff)
                sys.exit()
            # if j == 3:
            #     sys.exit()
    print(len(count1), len(profile_ids1))
    count1 = list(count1)
    count1.sort()
    populate(cli_parser, args, options, count1)
    # basically delete/reset the file
    with open("join_start_profiles.txt", "w") as f:
        f.write("")


def handler_non_thorough(cli_parser, args, options):
    "Join MP and C dbs on profile and depth non-thoroughly (fast)"
    table = options.table
    columns1, keys1, locations1,\
        profile_ids1, rows1 = get_profile_rows(args[0], table)
    # h.pprint(profile_ids)
    columns2, keys2, locations2,\
        profile_ids2, rows2 = get_profile_rows(args[1], table)
    print("Number of rows in both files:")
    print(len(rows1), len(rows2))
    rows1_old = rows1
    rows2_old = rows2
    rows1 = []
    rows2 = []
    N = 3
    for i in range(len(rows1_old)):
        rows1.append(rows1_old[i:i+N-1])
    for i in range(len(rows2_old)):
        rows2.append(rows2_old[i:i+N-1])
    count0 = []
    count1 = []
    count2plus = []
    for i, row in enumerate(rows1):
        c = rows2.count(row)
        new_row = (locations1[i], profile_ids1[i], c, row)
        if c == 0:
            count0.append(new_row)
        elif c == 1:
            index = rows2.index(row)
            count1.append((profile_ids1[i], profile_ids2[index]))
        elif c > 1:
            # print(profile_ids1[i], i, row)
            index = rows2.index(row)
            count2plus.append([locations2[index],
                               profile_ids2[index], new_row])
        else:
            raise Exception()
    h.pprint(count0)
    # h.pprint(count1)
    h.pprint(count2plus)
    print(len(count0), len(count1), len(count2plus))
    print("Combining uniquely matching rows "
          "(%d total out of %d)..."%(len(count1), len(rows1)))
    populate(cli_parser, args, options, count1)

def handler_plain_join(cli_parser, args, options):
    table = options.table
    columns1, keys1, locations1,\
        profile_ids1, rows1 = get_profile_rows(args[0], table)
    # h.pprint(profile_ids)
    columns2, keys2, locations2,\
        profile_ids2, rows2 = get_profile_rows(args[1], table)
    print("Number of rows in both files:")
    print(len(rows1), len(rows2))
    if len(rows1) != len(rows2):
        # h.pprint(rows1[:10])
        # h.pprint(rows2[:10])
        print(h.difflists(rows1, rows2))
        raise Exception("Number of rows in both files has to be same.")

    count1 = []
    for i, row in enumerate(rows1):
        count1.append((profile_ids1[i], profile_ids2[i]))
    populate(cli_parser, args, options, count1)

def handler_join_by_section(cli_parser, args, options):
    "Non-reliable as it doesn't deal with duplicate rows"
    global IGNORE_DIFFERING_NUMBER_OF_HORIZONTS
    IGNORE_DIFFERING_NUMBER_OF_HORIZONTS = False
    table = options.table
    con = sqlite3.connect(":memory:", detect_types=sqlite3.PARSE_DECLTYPES,
                                      check_same_thread=False,
                                      isolation_level=None)

    columns1, types1, rows1 = h.get_columns_rows(args[0], table)
    columns2, types2, rows2 = h.get_columns_rows(args[1], table)

    h.create_db(con, "T1", list(zip(columns1, types1)), rows1)
    h.create_db(con, "T2", list(zip(columns2, types2)), rows2)

    _1, _2, intersection = h.get_columns_rows_for_sql(con, "select distinct * from (select section, square, profile_prefix, profile_number, profile_suffix, profile_number2_number, profile_suffix2 from T1 intersect select section, square, profile_prefix, profile_number, profile_suffix, profile_number2_number, profile_suffix2 from T2)")
    _1, _2, rows1 = h.get_columns_rows_for_sql(con, "select distinct my_profile_id, section, square, profile_prefix, profile_number, profile_suffix, profile_number2_number, profile_suffix2 from T1")
    _1, _2, rows2 = h.get_columns_rows_for_sql(con, "select distinct my_profile_id, section, square, profile_prefix, profile_number, profile_suffix, profile_number2_number, profile_suffix2 from T2")
    keys1 = dict(((row[1:], row[0]) for row in rows1))
    keys2 = dict(((row[1:], row[0]) for row in rows2))
    count1 = []
    for key in intersection:
        # problematic section/square/profile combination
        if key[:4] == ("cetinje2", "f8", None, 138):
            continue
        count1.append((keys1[key], keys2[key]))
    populate(cli_parser, args, options, count1)


def handler_join_by_xy(cli_parser, args, options):
    "Most reliable as it deals with precise coordinates. Requires x and y columns to be present"
    global IGNORE_DIFFERING_NUMBER_OF_HORIZONTS
    IGNORE_DIFFERING_NUMBER_OF_HORIZONTS = False
    table = options.table
    con = sqlite3.connect(":memory:", detect_types=sqlite3.PARSE_DECLTYPES,
                                      check_same_thread=False,
                                      isolation_level=None)

    columns1, types1, rows1 = h.get_columns_rows(args[0], table)
    columns2, types2, rows2 = h.get_columns_rows(args[1], table)

    h.create_db(con, "T1", list(zip(columns1, types1)), rows1)
    h.create_db(con, "T2", list(zip(columns2, types2)), rows2)

    _1, _2, intersection = h.get_columns_rows_for_sql(con, "select distinct x, y from T1 where x is not null intersect select distinct x, y from T2 where x is not null")
    _1, _2, rows1 = h.get_columns_rows_for_sql(con, "select distinct my_profile_id, x, y from T1 where x is not null")
    _1, _2, rows2 = h.get_columns_rows_for_sql(con, "select distinct my_profile_id, x, y from T2 where x is not null")
    keys1 = dict(((row[1:], row[0]) for row in rows1))
    keys2 = dict(((row[1:], row[0]) for row in rows2))
    count1 = []
    for key in intersection:
        count1.append((keys1[key], keys2[key]))
    populate(cli_parser, args, options, count1)


def populate(cli_parser, args, options, pairs):
    pairs_old = pairs
    pairs = []
    duplicates = []
    e1s = set()
    e2s = set()
    for row in pairs_old:
        e1, e2 = row
        if (e1 in e1s) or (e2 in e2s):
            duplicates.append(row)
        else:
            pairs.append(row)
            e1s.add(e1)
            e2s.add(e2)
    print("These are the duplicates that were found:")
    pprint(duplicates)
    table = options.table

    prefix1 = os.path.basename(args[0]).split("_")[0]
    con1 = sqlite3.connect(args[0])
    cur1 = con1.cursor()
    types1 = [i[2] for i in cur1.execute("PRAGMA table_info(%s)"%table).fetchall()]
    columns1 = [i[1] for i in cur1.execute("PRAGMA table_info(%s)"%table).fetchall()]

    prefix2 = os.path.basename(args[1]).split("_")[0]
    con2 = sqlite3.connect(args[1])
    cur2 = con2.cursor()
    types2 = [i[2] for i in cur2.execute("PRAGMA table_info(%s)"%table).fetchall()]
    columns2 = [i[1] for i in cur2.execute("PRAGMA table_info(%s)"%table).fetchall()]

    cmd_select = "select %s " + "from %s"%table
    cmd_select += """
where my_profile_id=%s
order by my_row_id asc
"""

    if options.outfile:
        outfile = options.outfile
    else:
        outfile = prefix1 + prefix2 + ".sqlite"

    con = sqlite3.connect(outfile)
    cur = con.cursor()
    cur.execute("DROP TABLE IF EXISTS %s"%table)
    cmd_create = "create table IF NOT EXISTS %s"%table
    cmd_create += "(\n%s,\n\n%s)"
    columns1_new = [prefix1 + "_" + c for c in columns1]
    _cols1 = ",\n".join(["%s %s"%(c, t) for c, t in zip(columns1_new, types1)])
    columns2_new = [prefix2 + "_" + c for c in columns2]
    _cols2 = ",\n".join(["%s %s"%(c, t) for c, t in zip(columns2_new, types2)])
    cmd_create = cmd_create%(_cols1, _cols2)
    # print(cmd_create)
    cur.execute(cmd_create)
    cmd_insert = "INSERT INTO %s"%table
    cmd_insert += """(%s)
                VALUES
                (%s)"""%(",\n".join(columns1_new + columns2_new),
                         ", ".join(["?"]*len(columns1_new + columns2_new)))
    my_profile_ids1_paired_dict = dict(pairs)
    my_profile_ids2 = my_profile_ids1_paired_dict.values()
    my_profile_id2_old = -1
    my_profile_ids2_inserted = []
    def populate_rows2(my_profile_id2):
        nonlocal my_profile_id2_old

        ci1 = h.get_index(columns1_new)
        ci2 = h.get_index(columns2_new)


        _rows2 = cur2.execute("select * from %s where "
                          "my_profile_id > %d and "
                          "my_profile_id < %d "
                          ""%(table, my_profile_id2_old, my_profile_id2)).fetchall()
        for _row2 in _rows2:
            if my_profile_id2_old != _row2[ci2("my_profile_id")]:
                my_profile_ids2_inserted.append(my_profile_id2_old)
                my_profile_id2_old = _row2[ci2("my_profile_id")]
            if _row2[ci2("my_profile_id")] not in my_profile_ids2 and \
                _row2[ci2("my_profile_id")] not in my_profile_ids2_inserted:
                _row1 = [None,]*len(columns1_new)
                _row1[ci1("section"):ci1("depth_description")+1] =\
                    _row2[ci2("section"):ci2("depth_description")+1]

                _row1[ci1("section2")] = _row2[ci2("section2")]
                _row1[ci1("square")] = _row2[ci2("square")]

                _row1[ci1("x2")] = _row2[ci2("x2")]
                _row1[ci1("y2")] = _row2[ci2("y2")]
                # Modify the copied my_profile_id as it's misleading
                # _row1[ci1("my_profile_id")] = None
                _row1[ci1("my_profile_id")] += h.BIG_NUMBER
                cur.execute(cmd_insert,
                            _row1 + list(_row2))
        my_profile_id2_old = my_profile_id2

    for my_profile_id1 in cur1.execute("select distinct my_profile_id from "
                                       "%s"%(table)).fetchall():
        my_profile_id1 = my_profile_id1[0]
        _rows1 = cur1.execute(
            cmd_select%(", ".join(columns1), my_profile_id1)).fetchall()
        if my_profile_id1 in my_profile_ids1_paired_dict:
            my_profile_id2 = my_profile_ids1_paired_dict[my_profile_id1]
            # Insert rows from db2 that are not matched/joined, if any
            populate_rows2(my_profile_id2)
            # Now insert rows that are matched
            _rows2 = cur2.execute(
                cmd_select%(", ".join(columns2), my_profile_id2)).fetchall()
            my_profile_ids2_inserted.append(my_profile_id2)
            if len(_rows1) != len(_rows2):
                print("These two profiles have a differing number of layers")
                print(my_profile_id1, my_profile_id2)
                if IGNORE_DIFFERING_NUMBER_OF_HORIZONTS:
                    continue
                else:
                    h.show_my_profile_id(args[0], my_profile_id1)
                    h.show_my_profile_id(args[1], my_profile_id2)
                    sys.exit()
            for i in range(len(_rows1)):
                cur.execute(cmd_insert, _rows1[i] + _rows2[i])
            my_profile_id2_old = my_profile_id2
        else:
            # Insert rows from db1 that are not matched/joined
            for _row1 in _rows1:
                cur.execute(cmd_insert,
                            _row1 + (None,)*len(columns2_new))
    # Finally, insert any remaining non-matched rows from db2
    my_profile_id2 = cur2.execute("select max(my_profile_id) from %s"%table
                                  ).fetchall()[0][0] + 1
    populate_rows2(my_profile_id2)

    con.commit()
    con.close()


def get_profile_rows(sqlitefile, table="properties"):
    dbconn = sqlite3.connect(sqlitefile,
                             detect_types=sqlite3.PARSE_DECLTYPES,
                             check_same_thread=False)
    #dbconn.text_factory = str
    cursor = dbconn.cursor()

    types = [i[2] for i in cursor.execute("PRAGMA table_info(%s)"%table).fetchall()]
    columns = [i[1] for i in cursor.execute("PRAGMA table_info(%s)"%table).fetchall()]
    ci = columns.index

    _tmp = "book, page, page_row, location, my_profile_id, " \
           "profile_prefix, profile_number, profile_suffix, " \
           "profile_number2_number, profile_suffix2, " \
           "depth_min"#, depth_max"
    mycolumns = _tmp.split(", ")
    # print(mycolumns)
    mci = mycolumns.index
    sql = """
select %s
from %s order by my_row_id
"""%(_tmp, table)
    rows = cursor.execute(sql)
    keys = []
    key_old = None
    locations = []
    location_old = None
    my_profile_id_old = None
    profiles = []
    profiles_rows = []
    same_profile_rows = []
    for i, row in enumerate(rows):
        my_profile_id = row[mci("my_profile_id")]
        new_row = row[mci("my_profile_id")+1:]
        if my_profile_id != my_profile_id_old:
            if i != 0:
                keys.append(key_old)
                locations.append(location_old)
                profiles_rows.append(same_profile_rows)
                profiles.append(my_profile_id_old)
            key_old = (row[mci("book")],
                       row[mci("page")],
                       row[mci("page_row")])
            location_old = row[mci("location")]
            my_profile_id_old = my_profile_id
            same_profile_rows = [new_row]
        else:
            same_profile_rows.append(new_row)
    # Last row
    keys.append(key_old)
    locations.append(location_old)
    profiles_rows.append(same_profile_rows)
    profiles.append(my_profile_id_old)
    return mycolumns, keys, locations, profiles, profiles_rows

if __name__ == "__main__":
    # ext is not used at all
    ext = ".%s.sqlite"%os.path.splitext(os.path.basename(__file__))[0]
    # handler_non_thorough is faster and incorrect, handler_thorough is more thorough
    # and more correct
    handler = handler_join_by_xy
    cli_parser = h.create_cli_parser()
    cli_parser, args, options = h.parse_cli_parser(cli_parser, ext)
    if options.handler is not None:
        handler = globals()[options.handler]
    handler(cli_parser, args, options)

