import sys
import os
import sqlite3

import helpers as h

EXT = ".%s.sqlite"%os.path.splitext(os.path.basename(__file__))[0]

def handler(cli_parser, args, options):

    con = sqlite3.connect(args[0])

    my_profile_id = int(args[1])
    x2 = args[2]
    if x2.endswith(","):
        x2 = x2[:-1]

    y2 = args[3]
    if y2.endswith(","):
        y2 = y2[:-1]

    columns = [i[1] for i in con.execute("PRAGMA table_info(properties)").fetchall()]
    prefix = h.get_columns_prefix(columns)
    if prefix == "MP_":
        other_prefix = "C_"
    elif prefix == "C_":
        other_prefix = "MP_"
    elif prefix == "":
        other_prefix = ""
    else:
        raise Exception("Unrecognized prefix")

    sql_update = """
update properties
set {prefix}x2={x2}, {prefix}y2={y2}
""".format(prefix=prefix, x2=x2, y2=y2)
    if other_prefix != "":
        sql_update += ", {other_prefix}x2={x2}, {other_prefix}y2={y2}\n".format(other_prefix=other_prefix, x2=x2, y2=y2)

    sql_update += "where MP_my_profile_id=%d"%my_profile_id
    con.execute(sql_update)
    con.commit()
    con.close()


if __name__ == "__main__":
    h.main(ext=EXT,
           handler=handler)