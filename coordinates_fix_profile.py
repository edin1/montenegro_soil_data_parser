import os
import sqlite3
import helpers as h
from collections import OrderedDict as OD

def fix(cli_parser, args, options):
    columns, types, rows = h.get_columns_rows(args[0], options.table)
    ci = columns.index

    columns_new = columns[:]
    columns_new.insert(ci("profile"), "my_profile_id")
    types_new = types[:]
    types_new.insert(ci("profile"), "INTEGER")

    rows_new = []
    my_row_id = 0
    for row in rows:
        if row[ci("problem")]:
            if row[ci("problem")].lower() in ["empty profile"]:
                # problematic row, skip it
                continue
        row_new = list(row)
        my_row_id += 1
        row_new.insert(ci("profile"), my_row_id)
        rows_new.append(row_new)
    h.create_db(options.outfile, "properties", list(zip(columns_new, types_new)), rows_new)

if __name__ == "__main__":
    h.main(ext=".fix_profile.sqlite", handler=fix)