import os
import sys
import sqlite3
import csv
import helpers as h

COLUMNS = ["SQLite file", "table", "Total profiles",
    "Total rows/horizons"]

CELLS = []

def main():
    # con = sqlite3.connect(f, detect_types=sqlite3.PARSE_DECLTYPES,
    #                          check_same_thread=False)
    fname = sys.argv[0].rsplit(".", 1)[0] + ".csv"
    with open(fname, 'w', encoding='utf8') as csvfile, sqlite3.connect(sys.argv[1]) as con:
        csvwriter = csv.writer(csvfile)
        process(con, csvwriter)


def process(con, csvwriter):
    def myprint(*elements):
        csvwriter.writerow(elements)

    def myselect(columns="count(*)", table="properties", condition="1=1"):
        cmd = "select %s from %s where %s"%(columns, table, condition)
        return con.execute(cmd).fetchone()[0]

    for args in [
        ("Ukupan broj profila", "count(*)", "(select distinct MP_my_profile_id from properties)", "1=1"),
        ("Ukupan broj horizonata",),
        ("Ukupan broj georeferenciranih profila", "count(*)", "(select distinct MP_my_profile_id from properties where x is not null)"),
        ("Ukupan broj georeferenciranih horizonata", "count(*)", "(select MP_my_profile_id from properties where x is not null)"),
        ("Ukupan broj profila sa MP karakteristikama", "count(*)", "(select distinct MP_my_profile_id from properties where MP_book is not null)"),
        ("Ukupan broj horizonata sa MP karakteristikama", "count(*)", "(select MP_my_profile_id from properties where MP_book is not null)"),
        ("Ukupan broj georeferenciranih profila sa MP karakteristikama", "count(*)", "(select distinct MP_my_profile_id from properties where MP_book is not null and x is not null)"),
        ("Ukupan broj georeferenciranih horizonata sa MP karakteristikama", "count(*)", "(select MP_my_profile_id from properties where MP_book is not null and x is not null)"),
        ("Ukupan broj profila sa C karakteristikama", "count(*)", "(select distinct C_my_profile_id from properties where C_book is not null)"),
        ("Ukupan broj horizonata sa C karakteristikama", "count(*)", "(select C_my_profile_id from properties where C_book is not null)"),
        ("Ukupan broj georeferenciranih profila sa C karakteristikama", "count(*)", "(select distinct C_my_profile_id from properties where C_book is not null and x is not null)"),
        ("Ukupan broj georeferenciranih horizonata sa C karakteristikama", "count(*)", "(select C_my_profile_id from properties where C_book is not null and x is not null)"),
        # ("Ukupan broj profila i sa MP i sa C karakteristikama", "count(*)", "(select distinct C_my_profile_id from properties where C_book is not null and MP_book is not null)"),
        # ("Ukupan broj horizonata i sa MP i sa C karakteristikama", "count(*)", "(select C_my_profile_id from properties where C_book is not null and MP_book is not null)"),
        ("Ukupan broj georeferenciranih profila i sa MP i sa C karakteristikama", "count(*)", "(select distinct C_my_profile_id from properties where C_book is not null and MP_book is not null and x is not null)"),
        ("Ukupan broj georeferenciranih horizonata i sa MP i sa C karakteristikama", "count(*)", "(select C_my_profile_id from properties where C_book is not null and MP_book is not null and x is not null)"),
    ]:
        value = myselect(*args[1:])
        myprint(args[0], value)

if __name__ == "__main__":
    main()
