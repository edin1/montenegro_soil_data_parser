import os
import sqlite3
import helpers as h

def fix(cli_parser, args, options):
    columns, types, rows = h.get_columns_rows(args[0], options.table)
    dbconn = sqlite3.connect(options.outfile,
                             detect_types=sqlite3.PARSE_DECLTYPES,
                             check_same_thread=False)
    #dbconn.text_factory = str
    cursor = dbconn.cursor()

    cursor.execute("DROP TABLE IF EXISTS properties")

    cmd = """CREATE TABLE IF NOT EXISTS properties(
            %s
    )
    """%(",\n".join(["%s %s"%(c, t) for c, t in zip(columns, types)]))
    # print(cmd)
    cursor.execute(cmd)

    cmd = """INSERT INTO properties
                (%s)
                VALUES
                (%s)"""%(", ".join(columns), ", ".join(["?"]*len(columns)))


    ci = columns.index

    columns_decimal = []
    for c in columns:
        if c.endswith("_eq_sign"):
            c_v = c[:-len("_eq_sign")] + "_value"
            columns_decimal.append((c, c_v))

    for row in rows:
        new_row = list(row)
        for c in columns_decimal:
            if new_row[ci(c[0])] is not None:
                # if there is an _eq_sign (e.g. "<") set the number to None/null
                new_row[ci(c[0])] = None
                new_row[ci(c[1])] = None
        cursor.execute(cmd, new_row)
    dbconn.commit()
    dbconn.close()

if __name__ == "__main__":
    h.main(ext=".%s.sqlite"%os.path.splitext(os.path.basename(__file__))[0],
           handler=fix)