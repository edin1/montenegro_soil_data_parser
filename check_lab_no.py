import os
import sqlite3
import helpers as h

def myconvert(e):
    if e is not None:
        return e
    else:
        return ""

def fix(cli_parser, args, options):
    columns, types, rows = h.get_columns_rows(args[0], options.table)
    ci = columns.index

    old_row = [None]*len(columns)
    old_row[ci("lab_no")] = 0 # Initialize to zero because of comparison
    for row in rows:
        new_row = list(row)
        if new_row[ci("lab_no")] < old_row[ci("lab_no")]:
            if new_row[ci("profile")] is None:
                print(new_row)
                print(old_row)
                raise Exception("Old lab_no is greater than new. "
                                "This should be a new profile field, "
                                "but it's not.")
        old_row = new_row


if __name__ == "__main__":
    h.main(handler=fix)