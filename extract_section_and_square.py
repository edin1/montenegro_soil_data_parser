import os
import sys
import csv
import re
import xlrd
import helpers as h

def get_row(sheet, row):
    values = []
    for col in range(sheet.ncols):
        values.append(get_string(sheet, row, col))
    return values

def parse_row(sheet, row):
    pass

def get_string(sheet, row, col):
    try:
        cell = sheet.cell(row, col)
    except IndexError:
        return None
        # print(sheet.cell(row, col-1))
        # print(dir(sheet), row, col)
        # raise
    v = cell.value
    if cell.ctype == 3:
        print("%s%d"%(chr(ord("A") + col), row+1), repr(v))
        raise Exception("DATE type found")
    v = str(v).strip()#.lower()
    if v == "":
        return None
    v = " ".join(v.split())
    return v

def main():
    fname = sys.argv[1]
    fname_abs = os.path.abspath(fname)
    fname_base = os.path.basename(fname_abs)

    fname_new, ext = os.path.splitext(fname_abs)
    fname_new += "_new.csv"

    wb = xlrd.open_workbook(fname_abs)
    sheet = wb.sheets()[0]

    # print(dir(sheet))

    rows = []
    rows_for_profile_previous = []
    profile_previous = None
    for row in range(sheet.nrows):
        values = get_row(sheet, row)
        location_long = values[1]
        def pre_continue():
            values.insert(2, None)
            values.insert(2, None)
            rows.append(values)
        if location_long:
            location_long_reversed = location_long[::-1].lower()
            # print(location_long_reversed)
            m_square = re.match(r'(\d+\-?1?[a-z])(.*)', location_long_reversed)
            if not m_square:
                pre_continue()
                # print(row, '\n', values)
                continue
            square = "".join(m_square.groups()[0].replace("-", "").split())
            section_index = re.search(r"(\d|šej)", m_square.groups()[1])
            if section_index is None:
                pre_continue()
                print(row, '\n', values)
                continue

            section_index = section_index.start()
            section_location = m_square.groups()[1][section_index:]
            # print(section_location)
            # Try to guess section
            guessed = False
            for s in h.SECTIONS:
                s_r = s[::-1].lower()
                s_r = s_r.replace(" ", r"\s*")
                pattern = "(%s)(.*)"%s_r
                # print(pattern)
                section_location_index = re.search(pattern, section_location)
                if section_location_index is not None:
                    guessed = True
                    m_section_location = re.match(pattern, section_location[section_location_index.start():])
                    section = m_section_location.groups()[0]
                    location = m_section_location.groups()[1]
            if not guessed:
                pre_continue()
                print(row, '\n', values)
                continue
            location = location.replace(",", "", 1).strip()[::-1]
            if location.endswith("-"):
                location = location[:-1]
            section = section[::-1].strip()
            square = square[::-1].strip().replace("-", "")
            values.pop(1)
            #values.insert(1, section + "(%s)"%square)
            values.insert(1, square)
            values.insert(1, section)
            values.insert(1, location)
            # print(m.groups()[0])
            # print(m.groups()[1])
            #print(location_long)
        else:
            values.insert(1, None)
            # values.insert(1, None)
        rows.append(values)

    with open(fname_new, 'w', encoding='utf8') as csvfile:
        csvwriter = csv.writer(csvfile, lineterminator='\n')
        for row in rows:
            row_new = [h.myconvert(e) for e in row]
            csvwriter.writerow(row_new)


if __name__ == "__main__":
    main()