import os
from optparse import OptionParser
import sqlite3
import helpers as h

def mergesqlitefiles(files, outfile):
    rows_of_rows = []
    for i, file in enumerate(files):
        columns, types, rows = h.get_columns_rows(file, 'properties')
        if i == 0: # first file
            columns_types = (columns, types)
        if columns_types != (columns, types):
            raise Exception("File %s (number %d) has a different "
                            "column layout than previous file(s)."%
                            (repr(file), i))
        rows_of_rows.append(rows)

    ci = h.get_index(columns)

    dbconn = sqlite3.connect(outfile,
                             detect_types=sqlite3.PARSE_DECLTYPES,
                             check_same_thread=False)
    #dbconn.text_factory = str
    cursor = dbconn.cursor()

    cursor.execute("DROP TABLE IF EXISTS properties")

    cmd = """CREATE TABLE IF NOT EXISTS properties(
            %s
    )
    """%(",\n".join(["%s %s"%(c, t) for c, t in zip(columns, types)]))
    # print(cmd)
    cursor.execute(cmd)

    cmd = """INSERT INTO properties
        (%s)
        VALUES
        (%s)"""
    rowcount = 0
    my_row_id = 0
    my_profile_id = 0
    my_profile_id_old = -1
    for rows in rows_of_rows:
        n = 0
        for row in rows:
            n += 1
            my_row_id += 1
            new_row = list(row)
            new_row[ci("my_row_id")] = my_row_id
            if new_row[ci("my_profile_id")] != my_profile_id_old:
                my_profile_id += 1
                my_profile_id_old = new_row[ci("my_profile_id")]
            if new_row[ci("my_profile_id")] > h.BIG_NUMBER:
                new_row[ci("my_profile_id")] = my_profile_id + h.BIG_NUMBER
            else:
                new_row[ci("my_profile_id")] = my_profile_id
            cursor.execute(cmd%(", ".join(columns),
                                ", ".join(["?"]*len(columns))), new_row)
        # We add the rowcount after we fetch all rows, as required by
        # Python DB API Spec
        rowcount += n
    dbconn.commit()
    rowcount_merged = cursor.execute(
        "select count(*) from properties").fetchall()[0][0]
    if rowcount != rowcount_merged:
        msg = "rowcount %d != rowcount_merged %d"%(rowcount, rowcount_merged)
        raise Exception(msg)
    else:
        print("%d rows merged in total."%rowcount_merged)
    dbconn.close()




def main():
    cli_parser = OptionParser(
        usage='usage: %prog [options] file1.sqlite file2.sqlite [...]'
        )
    cli_parser.add_option("-f", "--force", action="store_true",
                      dest="force", default=False,
                      help="Overwrite output file if it already exists.")
    cli_parser.add_option("-o", "--output", dest="output", default='output.sqlite',
                      help="Name of output file.")
    (options, args) = cli_parser.parse_args()

    if not args or len(args) < 2:
        cli_parser.error("You have to supply at least 2 sqlite files:\n%s"
                         %cli_parser.usage)
    # else:
    file_ = args[0]#.decode("utf-8")
    if os.path.exists(options.output):
        if options.force:
            os.remove(options.output)
        else:
            msg = "Output file %s already exists. Use --force (or -f) " \
              "to overwrite this file."%options.output
            cli_parser.error(msg)

    mergesqlitefiles(args, options.output)

if __name__ == "__main__":
    main()