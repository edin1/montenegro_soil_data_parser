import os
import sqlite3
import helpers as h

def fix(cli_parser, args, options):
    con = sqlite3.connect(args[0],
                             detect_types=sqlite3.PARSE_DECLTYPES,
                             check_same_thread=False)
    types = [i[2] for i in
        con.execute("PRAGMA table_info(%s)"%options.table).fetchall()]
    columns = [i[1] for i in
        con.execute("PRAGMA table_info(%s)"%options.table).fetchall()]

    ci = h.get_index(columns)
    #dbconn.text_factory = str
    con.execute("DROP VIEW IF EXISTS section_not_null_view")
    cmd = """
        CREATE VIEW section_not_null_view AS
        SELECT * FROM %s
        WHERE section IS NOT NULL;"""%options.table
    con.execute(cmd)
    con.commit()
    con.close()

if __name__ == "__main__":
    h.main(handler=fix)