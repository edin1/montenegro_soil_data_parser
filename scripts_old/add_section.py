import os
from collections import OrderedDict as OD
from pprint import pprint
import sqlite3
import xlrd

import helpers as h

def handler(cli_parser, args, options):
    columns, types, rows = h.get_columns_rows(args[0], options.table)
    ci = h.get_index(columns)

    con_toc = sqlite3.connect(args[1])

    # engine_coordinates = sa.create_engine('sqlite:///%s'%args[2], echo=False)
    # metadata = sa.MetaData()
    # Base = sa.ext.declarative.declarative_base()
    # Base.metadata = metadata

    columns_new = columns[:]
    types_new = types[:]
    columns_new = columns_new[:ci("my_profile_id")] +\
                  ["section"] +\
                    columns_new[ci("my_profile_id"):]
    types_new = types_new[:ci("my_profile_id")] +\
                  ["SECTION"] + types_new[ci("my_profile_id"):]
    rows_new = []

    cmd_select_toc = """
        select section from properties
        where book = ? and page_begin <= ? and page_end >= ?
        and lab_no_begin <= ? and lab_no_end >= ?
    """
    rows_toc_processed = []
    rows_missing_properties = []
    excel_rows_matched_coordinates = []
    rows_sections_dict = {}
    for row in rows:
        row_new = list(row)
        book = row[ci("book")]
        page = row[ci("page")]
        lab_no = row[ci("lab_no")]
        lab_no_suffix = row[ci("lab_no_suffix")]
        my_profile_id = row[ci("my_profile_id")]

        if my_profile_id in rows_sections_dict:
            row_coordinates = rows_sections_dict[my_profile_id]
        else:
            # lab_no = "".join([c for c in str(lab_no) if c.isdigit()])

            _values = [book, page, page, lab_no, lab_no]
            row_toc = con_toc.execute(cmd_select_toc,
                _values
            ).fetchall()
            row_toc = [_row for _row in row_toc if [
                _row[0], book, page, lab_no, lab_no_suffix] not in rows_toc_processed]
            if row_toc:
                row_toc = row_toc[0]
            else:
                row_new = row_new[:ci("my_profile_id")] + [None] +\
                          row_new[ci("my_profile_id"):]
                rows_new.append(row_new)
                continue
            section = row_toc[0]
            rows_toc_processed.append([
                section, book, page, lab_no, lab_no_suffix])
        row_new = row_new[:ci("my_profile_id")] + [section] + \
                      row_new[ci("my_profile_id"):]
        rows_new.append(row_new)
    h.create_db(options.outfile, "properties",
        list(zip(columns_new, types_new)), rows_new)


if __name__ == "__main__":
    h.main(ext=".%s.sqlite"%os.path.splitext(os.path.basename(__file__))[0],
           handler=handler)