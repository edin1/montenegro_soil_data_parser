import os
import sys
import itertools
from collections import OrderedDict as OD
import sqlite3
import csv
import time
import shutil

import matplotlib
import numpy as np
from sklearn import linear_model, svm
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import Pipeline
import scipy.stats

matplotlib.use('tkagg')
import matplotlib.pyplot as plt
plt.ion()

import helpers as h

SYS_STDOUT = sys.stdout

class MyPipeline(Pipeline):
    def __getattr__(self, attr):
        return getattr(self.named_steps["linear"], attr)

try:
    degree = sys.argv[3]
except IndexError:
    degree = 2
poly = PolynomialFeatures(degree=int(degree))

ML_MODELS = {
    "svr": svm.SVR(kernel='rbf', C=1e3, gamma=0.1),
    "ridge": linear_model.Ridge(),
    "linreg": linear_model.LinearRegression(),
    "poly": MyPipeline([('poly', poly),
            ('linear', linear_model.LinearRegression(fit_intercept=False))]),
}

N_ROWS_MIN = 50

def handler(cli_parser, args, options):
    with open(options.outfile, 'w') as f:
        old_stdout = sys.stdout
        t1 = time.strftime("%Y-%m-%d-%H-%M-%S")
        print(t1)
        sys.stdout = f
        print(sys.argv)
        print(open(__file__).read())
        _handler(cli_parser, args, options)
        t2 = time.strftime("%Y-%m-%d-%H-%M-%S")
        print()
        print("Started at: ", t1)
        print("Ended at: ", t2)
        sys.stdout = old_stdout
        print(t2)
    shutil.copyfile(options.outfile,
                    options.outfile.rsplit(".", 2)[0] + ".txt")

def _handler(cli_parser, args, options):
    dbconn = sqlite3.connect(args[0],
                             detect_types=sqlite3.PARSE_DECLTYPES,
                             check_same_thread=False)
    #dbconn.text_factory = str
    cursor = dbconn.cursor()

    types = [i[2] for i in
        cursor.execute("PRAGMA table_info(data_mining_view)").fetchall()]
    columns = [i[1] for i in
        cursor.execute("PRAGMA table_info(data_mining_view)").fetchall()]

    # Count non-null column members and sort them by count(*)
    # Sorting is needed in order to use the more numerous variables
    # as input values for less numerous values
    COUNTS = []
    for c in columns[1:]: # skip my_row_id
        cmd = "select count(*) from data_mining_view where %s is not null"%c
        count = cursor.execute(cmd).fetchone()[0]
        #if count > 0:
        COUNTS.append((count, c))
    COUNTS.sort()
    COUNTS.reverse()
    RESULTS = OD()
    for y_index in range(1, len(COUNTS)):
        cy = COUNTS[y_index][1]
        # Calculate a cy-centered count of dependant columns
        counts_x = []
        for c in [i[1] for i in COUNTS[:y_index]]: # skip my_row_id
            cmd = "select count(*) " \
                  "from data_mining_view " \
                  "where %s is not null " \
                  "and %s is not null"%(c, cy)
            count = cursor.execute(cmd).fetchone()[0]
            #if count > 0:
            counts_x.append((count, c))

            # cmd = """select my_row_id, %s
            #       from data_mining_view
            #       where %s
            #       order by my_row_id"""%(", ".join([c, cy]),
            #              " and ".join(["%s is not null"%i for i in [c, cy]]))
            # rows = cursor.execute(cmd).fetchall()
            # if len(rows) < N_ROWS_MIN:
            #     continue
            # score, lines = perform_regression(rows, args[1])
            # counts_x.append((score, c))
        counts_x.sort()
        counts_x.reverse()

        columns_that_lower_score = []

        # # 1. All combinations of input variables (i.e. those variables whose
        # # count is less than y's count)
        # _CX = [i[1] for i in counts_x]
        # CX = itertools.chain(*map(lambda x:
        #                           itertools.combinations(_CX, x),
        #                           range(1, len(_CX)+1)))
        # for cx in CX:
        #     cx = list(cx)
        # # 2. Adding input variables, one at a time
        CX = [i[1] for i in counts_x]
        for cx in [CX[:i] for i in range(1, len(CX)+1)]:
            for c in columns_that_lower_score:
                if c in cx:
                    cx.remove(c)
            if not cx:
                continue
            print(cx)
            print(cy)
            cmd = """select my_row_id, %s
              from data_mining_view
              where %s
              order by my_row_id"""%(", ".join(list(cx) + [cy]),
                " and ".join(["%s is not null"%i for i in list(cx) + [cy]]))

            rows = cursor.execute(cmd).fetchall()
            if len(rows) < N_ROWS_MIN:
                msg = "For %s there are less " \
                      "than %d (%d) non-null rows"%(cy, N_ROWS_MIN, len(rows))
                print(msg)
                print("-"*40)
                continue
                # raise Exception(msg)
            score, lines = perform_regression(rows, args[1])
            h.pprint(lines)
            if cy not in RESULTS or RESULTS[cy][0] < score:
                RESULTS[cy] = (score, cy, cx, lines)
            else:
                # pass
                columns_that_lower_score.append(cx[-1])
            print("-"*40)
        print("="*40)
    h.pprint(RESULTS)
    score_key = []
    for key in RESULTS:
        score_key.append(RESULTS[key])
    score_key.sort()
    score_key.reverse()
    for line in score_key:
        print(line)
    import csv
    with open('res.csv', 'w', newline='') as csvfile:
        w = csv.writer(csvfile)
        w.writerow(['y', 'Br. x varijabli', 'Br. ne-NULL redova', "Rezultat"])
        for line in score_key:
            w.writerow([line[1], len(line[2]),
                        line[3]["Number of non-null rows"], "%.2f"%line[0]])

def perform_regression(rows, ml_model):
    rows_number = {"Number of non-null rows": len(rows)}
    # print(rows_number)

    nxy = np.array(rows)
    shape = nxy.shape
    #h.pprint(counts)
    nxy_random = np.copy(nxy)
    #np.random.shuffle(nxy_random)
    index = np.round(nxy_random.shape[0]*0.7)
    # print(index)
    # print(counts)
    # print(nxy_random.shape)
    # print(nxy_random.shape[1])
    nxy_random_train = nxy_random[:index]
    nxy_random_test = nxy_random[index:]
    nx_train = nxy_random_train[:, 1:shape[1]-1]
    ny_train = nxy_random_train[:, shape[1]-1]
    # print(nx_train)
    # print(ny_train)

    nx_test = nxy_random_test[:, 1:shape[1]-1]
    ny_test = nxy_random_test[:, shape[1]-1]

    nx = nxy[:, 1:shape[1]-1]
    ny = nxy[:, shape[1]-1]
    # print(nx.shape, ny.shape)
    # scipy_values = scipy.stats.linregress(nx, ny)
    # Create linear regression object
    regr = ML_MODELS[ml_model]
    # Train the model using the training sets
    regr.fit(nx_train, ny_train)

    # The coefficients
    residual_sum_of_squares = \
        np.mean((regr.predict(nx_test) - ny_test) ** 2)
    score = regr.score(nx_test, ny_test)
    lines = []
    # lines.append("slope, intercept, r_value, p_value, std_err: %s" %
    #              repr(scipy_values))
    lines = OD(rows_number)
    lines.update({'Intercept': regr.intercept_})
    # lines.update({'Coefficients': regr.coef_})
    # The mean square error
    lines.update({"Residual sum of squares": residual_sum_of_squares})
    # Explained score: 1 is perfect prediction
    lines.update({'score': score})
    # for line in lines.items():
    #     print(line)
    return score, lines

if __name__ == "__main__":
    t = time.strftime("%Y-%m-%d-%H-%M-%S")
    h.main(ext=".results.%s.%s.txt"%("_".join(sys.argv[2:]), t),
           handler=handler)