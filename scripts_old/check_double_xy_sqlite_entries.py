import sqlite3
from pprint import pprint

import helpers as h

def check_double_MPC_entries(cli_parser, args, options):
    sqlfile = args[0]
    db = sqlite3.connect(sqlfile)
    sql = """
SELECT MP_my_profile_id, x, y
FROM properties
WHERE x is not null and y is not null
    """

    MP_my_profile_id_old = None
    xys = []
    MP_my_profile_ids = set()

    for row in db.execute(sql).fetchall():
        # print(row)
        MP_my_profile_id, x, y = row
        if MP_my_profile_id != MP_my_profile_id_old:
            if MP_my_profile_id not in MP_my_profile_ids:
                MP_my_profile_ids.add(MP_my_profile_id)
                MP_my_profile_id_old = MP_my_profile_id
                xys.append((x, y))
            else:
                print("Duplicate:", MP_my_profile_id)
        else:
            continue

    xys_set = set(xys)
    duplicates = []
    for row in xys:
        if row in xys_set:
            xys_set.remove(row)
        else:
            duplicates.append(row)
    assert len(xys_set) == 0
    pprint(duplicates)
    print(len(xys), len(duplicates))

    db.close()

if __name__ == "__main__":
    h.main(handler=check_double_MPC_entries)
