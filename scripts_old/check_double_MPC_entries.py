import sqlite3
from pprint import pprint

import helpers as h

def check_double_MPC_entries(cli_parser, args, options):
    sqlfile = args[0]
    db = sqlite3.connect(sqlfile)
    sql = """
SELECT MP_my_profile_id, MP_depth_min, MP_depth_max, C_my_profile_id
FROM properties
    """
    cur = db.execute(sql).fetchall()
    rows = []
    for row in cur:
        rows.append(row)
    db.close()
    rows_set = set(rows)
    duplicates = []
    for row in rows:
        if row in rows_set:
            rows_set.remove(row)
        else:
            duplicates.append(row)
    assert len(rows_set) == 0
    pprint(rows)
    pprint(duplicates)
    print(len(rows), len(duplicates))


if __name__ == "__main__":
    h.main(handler=check_double_MPC_entries)
