import os
import helpers as h
import xml.etree.ElementTree as et
from xml.dom import minidom
import xlrd

def handler(cli_parser, args, options):
    kml = et.Element('kml', xmlns="http://earth.google.com/kml/2.0")
    wb = xlrd.open_workbook(args[0])
    sheet = wb.sheets()[0]
    for row in range(sheet.nrows):
        def get_col(col):
            cell = sheet.cell(row, col)
            return str(cell.value)
        if row < 2:
            continue
        name = ", ".join([get_col(4), get_col(2), get_col(7)])
        x = get_col(5)
        y = get_col(6)
        placemark = et.SubElement(kml, "Placemark")
        et.SubElement(placemark, "name").text = name
        point = et.SubElement(placemark, "Point")
        et.SubElement(point, "coordinates").text = ",".join([x, y])
    rough_string = et.tostring(kml, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    with open("coordinates.kml", "w", encoding="utf-8") as f:
        f.write(reparsed.toprettyxml(indent="  "))



if __name__ == "__main__":
    h.main(ext=".%s.kml"%os.path.splitext(os.path.basename(__file__))[0],
           handler=handler)