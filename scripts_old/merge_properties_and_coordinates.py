import sys
import os
import sqlite3
import difflib


import helpers as h

def handler(cli_parser, args, options):
    file_properties = args[0]
    file_coordinates = args[1]
    table = options.table


if __name__ == "__main__":
    h.main(ext=".%s.sqlite"%os.path.splitext(os.path.basename(__file__))[0],
           handler=handler)