import csv
from pprint import pprint

import helpers as h

def check_double_csv_xy_entries(cli_parser, args, options):
    rows = []
    duplicates= []
    with open(args[0], 'r', encoding='utf8') as csvfile:
        reader = csv.reader(csvfile, lineterminator='\n')
        columns = next(reader)
        ci = columns.index
        for row in reader:
            rows.append((row[ci("x")], row[ci("y")]))
    rows_set = set(rows)
    for row in rows:
        if row in rows_set:
            rows_set.remove(row)
        else:
            duplicates.append(row)
    assert len(rows_set) == 0
    pprint(rows)
    pprint(duplicates)
    print(len(rows), len(duplicates))


if __name__ == "__main__":
    h.main(handler=check_double_csv_xy_entries)
