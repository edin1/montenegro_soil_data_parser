import os
from collections import OrderedDict as OD
from pprint import pprint
import sqlite3
import xlrd

import helpers as h

def handler(cli_parser, args, options):
    sections = get_sections(args[0])
    columns = [("book", "TEXT"),
               ("page_begin", "TEXT"), ("page_end", "TEXT"),
               ("section", "SECTION"),
               ("lab_no_begin", "INTEGER"), ("lab_no_end", "INTEGER"),
                ]
    h.create_db(options.outfile, "properties", columns, sections)

def get_sections(fname):
    wb = xlrd.open_workbook(fname)
    sheet = wb.sheets()[0]
    profile_tuples = OD()
    book = None
    sections = []
    for row in range(sheet.nrows):
        def get_col(col):
            cell = sheet.cell(row, col)
            cell = str(cell.value).strip()
            if cell in ["", None]:
                cell = None
            elif cell.endswith(".0"):
                cell = cell[:-2]

            return cell

        def get_section():
            section = get_col(2)
            if section is None:
                return section
            section = "".join(section.lower().split())
            for pair in [
                ("š", "s"),
                ("đ", "dj"),
                ("č", "c"),
                ("ć", "c"),
                ("ž", "z"),
            ]:
                section = section.replace(pair[0], pair[1])
            return section

        if get_col(0) in ["MP1", "MP2", "MP3"]:
            book = get_col(0)
        if book is None:
            continue
        section = get_section()
        if section:
            lab_no_begin = get_col(0)
            lab_no_end = get_col(1)
            page_begin = get_col(5)
            page_end = get_col(6)
            sections.append((book, "0"*(3-len(page_begin)) + page_begin,
                "0"*(3-len(page_end)) + page_end, section, lab_no_begin,
                lab_no_end))
    old_sections = sections[:]
    sections.sort()
    if old_sections != sections:
        raise Exception("It seems that the rows in the file %s "
                        "are not sorted"%fname)
    return sections

if __name__ == "__main__":
    h.main(ext=".sqlite",
           handler=handler)