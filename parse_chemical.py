# -*- coding: utf-8 -*-
import sys
import os
import io
import re
import sqlite3
import decimal
from optparse import OptionParser
from collections  import OrderedDict as OD
import difflib
import csv

import xlrd

if sys.version_info.major < 3:
    raise Exception("This script needs Python 3")

LINE = "-----------------------------"
D=decimal.Decimal
VALUES_DICT = {}

class InvalidRowException(Exception):
    pass

class InvalidRangeException(Exception):
    pass

def adapt_decimal(d):
    return str(d)

def convert_decimal(s):
    return D(s)

# Register the adapter
sqlite3.register_adapter(D, adapt_decimal)

# Register the converter
sqlite3.register_converter("decimal", convert_decimal)

class Page:
    def __init__(self, page):
        self.page = page

class Section:
    def __init__(self, section):
        self.section = section

def myint(s):
    # Try to match a pure int, i.e. 123
    try:
        if float(s) == int(float(s)):
            return int(float(s)), None
    except ValueError:
        pass

    # Try to match a int + a letter, i.e. 123a
    res = re.match("([0-9]+)[/]*\s*([a-zA-Z])", s)
    if res is not None:
        number = res.groups()[0]
        letter = res.groups()[1]
        return number, letter
    else:
        raise ValueError("The string %s doesn't look like a number"%repr(s))

def get_lab_no(s):
    # Remove whitespace if present
    s = s.strip()

    # Remove "." at the end if present
    if s.endswith("."):
        s = s[:-1]

    # this is for the situation where there's only one integer
    try:
        res = myint(s)
        return res
    except ValueError:
        pass

    # this is for the situation where there's a lab number and a "run" number
    # i.e. "2013 45"
    try:
        int1, int2 = s.split()
        int1 = myint(int1)
        int2 = myint(int2)

        return int1
    except ValueError:
        pass

    raise Exception("Cell contents were not recognized: %s"%repr(s))

def is_all_numbers(s):
    s = "".join(s.split())
    try:
        int(s)
    except:
        return False
    return True

COLUMNS = OD([
    ("location", "TEXT"),
    ("profile", "PROFILE"),
    ("depth", "RANGE"),
    ("H2O", "DECIMAL"),
    ("KCl", "DECIMAL"),
    ("CaCO3", "DECIMAL"),
    ("FaCa", "DECIMAL"),
    ("humus", "DECIMAL"),
    ("humus_character", "HUMUS_CHARACTER"),
    ("N", "DECIMAL"),
    ("P2O5", "DECIMAL"),
    ("K2O", "DECIMAL"),
    ("Y1", "DECIMAL"),
    ("S", "DECIMAL"),
    ("T", "DECIMAL"),
    ("T_S", "DECIMAL"),
    ("V", "DECIMAL"),
    ("H", "DECIMAL"),
    ("IDG", "DECIMAL"),
    ("FeO3", "DECIMAL"),
    ("section_and_square", "SECTION_AND_SQUARE"),
    ("P2O5_Al_method_1960", "DECIMAL"),
    ("K2O_Al_method_1960", "DECIMAL"),
    ("S_u_0_14_u_HCl", "DECIMAL"),
    ("CaO", "DECIMAL"),
    ("Other", "TEXT"),
    ("P2O5_total", "DECIMAL"),
    ("K2O_total", "DECIMAL"),
    ("x2", "COORDINATE"),
    ("y2", "COORDINATE"),
])

def expand_columns(columns):
    new_columns = []
    for c, t in columns.items():
        if t == "PROFILE":
            new_columns.append((c, "TEXT"))
            new_columns.append((c + "_prefix", "TEXT"))
            new_columns.append((c + "_number", "INTEGER"))
            new_columns.append((c + "_suffix", "TEXT"))
            new_columns.append((c + "_number2_number", "INTEGER"))
            new_columns.append((c + "_suffix2", "TEXT"))
        elif t == "RANGE":
            new_columns.append((c + "_min", "DECIMAL"))
            new_columns.append((c + "_max", "DECIMAL"))
            new_columns.append((c + "_description", "TEXT"))
        elif t == "DECIMAL":
            new_columns.append((c + "_value", "DECIMAL"))
            new_columns.append((c + "_eq_sign", "EQ_SIGN"))
            new_columns.append((c + "_description", "TEXT"))
        elif t == "WATER_PERMEABILITY":
            new_columns.append((c + "_cm", "DECIMAL"))
            # new_columns.append((c + "_minute", "DECIMAL"))
        elif t == "DORSEY_FACTOR":
            new_columns.append((c, "DECIMAL"))
        elif t == "SECTION_AND_SQUARE":
            new_columns.append(("section2", "TEXT"))
            new_columns.append(("square", "TEXT"))
        elif t == "STABILITY":
            new_columns.append((c, "TEXT"))
            new_columns.append((c + "_c2n", "DECIMAL"))
        elif t == "HUMUS_CHARACTER":
            new_columns.append((c, "TEXT"))
            new_columns.append((c + "_c2n", "DECIMAL"))
        else:
            new_columns.append((c, t))
    return OD(new_columns)

def convert_dir(dir_, args, options):
    if dir_.endswith("/"):
        dir_ = dir_[:-1]
    outfile = dir_ + ".sqlite"
    dbconn = sqlite3.connect(outfile,
                             detect_types=sqlite3.PARSE_DECLTYPES,
                             check_same_thread=False)
    #dbconn.text_factory = str
    cursor = dbconn.cursor()

    cursor.execute("DROP TABLE IF EXISTS properties")

    cmd = """CREATE TABLE IF NOT EXISTS properties(
            my_row_id INTEGER,
            book TEXT,
            page_begin TEXT,
            page_end TEXT,
            entry_person TEXT,
            excel_row INTEGER,
            page TEXT,
            page_row INTEGER,
            section SECTION,
            lab_no INTEGER,
            lab_no_suffix TEXT,
            -- cur_no TEXT,
            %s
    )
    """%(",\n".join(["%s %s"%(c, t) for c, t in expand_columns(COLUMNS).items()]))
    # print(cmd)
    cursor.execute(cmd)
    dbconn.commit()

    cursor = dbconn.cursor()
    columns = [i[1] for i in cursor.execute("PRAGMA table_info(properties)").fetchall()]
    last_row = [None]*len(columns)
    for file_ in sorted(os.listdir(dir_)):
        file_ = os.path.abspath(os.path.join(dir_, file_))
        if os.path.isfile(file_):
            last_row_old = last_row
            last_row = parse_file(file_, cursor, last_row)
            if last_row is None:
                last_row = last_row_old
    dbconn.commit()
    cursor = dbconn.cursor()
    sql = "SELECT DISTINCT page FROM properties"
    cursor.execute(sql)
    pages_covered = []
    for row in cursor:
        pages_covered.append(int(row[0]))
    if options.endpage:
        page_end = int(options.endpage)
    else:
        page_end = max(pages_covered)
    if options.beginpage:
        page_begin = int(options.beginpage)
    else:
        page_begin = min(pages_covered)
    print(page_end)
    pages_all = range(page_begin, page_end + 1)
    pages_missing = set(pages_all) - set(pages_covered)
    pages_missing = list(pages_missing)
    pages_missing.sort()
    with open(dir_ + ".missing.txt", "w") as f:
        f.write(", ".join((str(i) for i in pages_missing)))
        # f.write"\n%d"%
    dbconn.close()
    # sqlite2xls(outfile)

def parse_file(file_, cursor, values_old):
    types = [i[2] for i in cursor.execute("PRAGMA table_info(properties)").fetchall()]
    columns = [i[1] for i in cursor.execute("PRAGMA table_info(properties)").fetchall()]
    ci = columns.index

    def fix_values(values, values_old):
        for i in range(len(values)):
            value = values[i]
            if value == "-ii-":
                values[i] = values_old[i]
            if i == ci("location") and value in [None]:
                values[i] = values_old[i]
            # if i == ci("section2") and value in [None] and \
            #     values[ci("profile")]==values_old[ci("profile")]:
            #     if values_old[i] is not None:
            #         print(values_old[i])
            #     values[i] = values_old[i]
            # if i == ci("square") and value in [None] and \
            #     values[ci("profile")]==values_old[ci("profile")]:
            #     values[i] = values_old[i]
            #else:
            #    values[i] = values[i]


    pfile = file_.encode("utf-8")
    print(pfile)
    basename, ext = os.path.basename(file_).rsplit(".", 1)
    if ext.lower() not in ('xls', 'xlsx'):
        return
    # else:
    try:
        book, pages, entry_person = basename.split(", ")
        print(pages)
        page_begin, page_end = pages.split()[1].split('-')
        page_begin = "%03d"%int(page_begin)
        page_end = "%03d"%int(page_end)
    except ValueError:
        print("No book data in the filename")
        entry_person = basename
        book, page_begin, page_end = [None]*3
        # raise
    wb = xlrd.open_workbook(file_)
    sheet = wb.sheets()[0]
    old_page = "000"
    page = "000"
    page_row = 0
    old_section = None
    section = None
    my_row_id = values_old[0]
    if my_row_id is None:
        my_row_id = 0
    _values_old = [None]*len(COLUMNS)
    for row in range(sheet.nrows):
        try:
            _values = parse_row(row, sheet, pfile)
        except InvalidRowException:
            # print("Invalid row")
            continue
        if _values is None:
            continue
        elif isinstance(_values, Page):
            old_page = page
            page = _values.page
            page_row = 0
            if page < old_page:
                raise Exception("New page %s is lower than previous %s"%(
                    page, old_page)
                )
            continue
        elif isinstance(_values, Section):
            old_section = section
            section = _values.section
            continue
        # else: an actual row with soil data
        if _values == _values_old:
            print(_values, _values_old)
            print(len(_values), len(_values_old))
            raise Exception("Row is identical to previous.")
        _values_old = _values
        my_row_id += 1
        page_row += 1
        values = [my_row_id, book, page_begin, page_end, entry_person,
                  row+1, page, page_row, section]
        values.extend(_values)
        if section is not None:
            if values[ci("section2")] is not None:
                if section != values[ci("section")]:
                    raise Exception("Tagged and cell-specified section are different!")
                # else: pass
        elif values[ci('section2')] is not None:
            values[ci("section")] = values[ci('section2')]
        # if "clay_value" in columns and values[ci("clay_value")] is not None and values[ci("sand_value")] is not None:
        #     _sum = int(values[ci("clay_value")] + values[ci("sand_value")])
        #     if _sum < 99:
        #         print(file_)
        #         print("Row:", row+1)
        #         msg = "clay + sand has to be 100, but it is %d"%_sum
        #         raise Exception(msg)
        key = (book, page, page_row)
        if key in VALUES_DICT:
            # We need the my_row_id of the first row that was input
            my_row_id = VALUES_DICT[key][0]
            # Diff of stored and current value
            r1 = list(zip(columns, VALUES_DICT[key]))
            r2 = list(zip(columns, values))
            r1.pop(ci("location"))
            r2.pop(ci("location"))
            # The below page index works because location comes after it
            t1 = [str(i) for i in r1[ci("page"):]]
            t2 = [str(i) for i in r2[ci("page"):]]
            if t1 != t2:
                print(values[ci("book")])
                print(values[ci("page")])
                print(values[ci("profile")])
                print("New entry person:", values[ci("entry_person")])
                print("Old entry person:",
                      VALUES_DICT[key][ci("entry_person")])
                print(values[ci("page")])
                print(values[ci("lab_no")])
                print("\n".join(difflib.unified_diff(t1, t2)))
                # Filip Pejović was the person who was
                # responsible for inputing double-check data
                # TODO more sophisticated comparing, right now (mostly) just pick
                # TODO what ever Filip Pejović input as more correct
                if r2[ci("entry_person")] == "Filip Pejović":
                    if r1[ci("entry_person")] in ("Amar Rastoder",):
                        cmd = """UPDATE properties
                                 SET
                                 %s
                                 WHERE
                                 my_row_id = %s;
                                 """%(["%s = %s,\n"%(i, j) for i,j in zip(keys, ["'?'"]*len(values))],
                                      my_row_id
                                      )
                        print(cmd)
                        values[0] = my_row_id
                        fix_values(values, values_old)
                        values_old = values
                        cursor.execute(cmd, values)
                        VALUES_DICT[key] = values
                        continue
                    else:
                        raise Exception("book:", values[ci("book")], "page:", values[ci("page")], "profile:", values[ci("profile")])
                        continue
                else:
                    continue
            else: # We don't insert duplicate rows in the db
                continue
        # else:
        fix_values(values, values_old)
        values_old = values
        VALUES_DICT[key] = values
        keys = "my_row_id, book, page_begin, page_end, entry_person, " \
               "excel_row, page, page_row, section, lab_no, " \
               "lab_no_suffix, " + ", ".join(expand_columns(COLUMNS).keys())
        cmd = """INSERT INTO properties
                (%s)
                VALUES
                (%s)"""%(keys, ", ".join(["?"]*len(values)))
        cursor.execute(cmd, values)
    return values_old

def get_string(sheet, row, col):
    try:
        cell = sheet.cell(row, col)
    except IndexError:
        return ""
        # print(sheet.cell(row, col-1))
        # print(dir(sheet), row, col)
        # raise
    v = cell.value
    if cell.ctype == 3:
        print("%s%d"%(chr(ord("A") + col), row+1), repr(v))
        raise Exception("DATE type found")
    v = str(v).strip().lower()
    v = " ".join(v.split())
    v = v.replace(" - ", "-")
    v = v.replace("- ", "-")
    v = v.replace(" -", "-")
    v = v.replace("-ll-", "-ii-")
    if v in ["ll", "ll-", "-ll", "-ii", "ii-"]:
        v = "-ii-"
    if v.replace("-ii-", "").strip() == "" and v.strip() != "": # two or more "-ii-"
        v = "-ii-"
    if not v.lower().replace("'", "").replace('"', '').replace("#", "").replace(" ", ""):
        v = ""
    return v

def get_page(sheet, row, col):
    v = get_string(sheet, row, col).lower()
    res = re.match("str\s*([0-9]+)", v)
    if res:
        page = res.groups()[0]
        return Page("%03d"%int(page))

def section_simplify(section):
    section = "".join(section.split()).lower()
    section = convert_to_ascii(section)
    return section

def convert_to_ascii(text):
    for pair in [
            ("š", "s"),
            ("đ", "dj"),
            ("č", "c"),
            ("ć", "c"),
            ("ž", "z"),
        ]:
            text = text.replace(pair[0], pair[1])
    return text

def get_section(sheet, row, col):
    v = get_string(sheet, row, col).lower()
    res = re.match("(?:section|sekcija)\s+(.*)", v)
    if res:
        section = section_simplify(res.groups()[0])

        if section in ["none", "prazno", "prazna",
                       "ne postoji", "nema", "ne", "nista"]:
            section = None
        return Section(section)

def get_LAB_NO(sheet, row, col):
    cell = sheet.cell(row, col)
    cell00 = cell.value
    v = get_string(sheet, row, col)
    # if isinstance(cell00, str):
    #     cell00 = cell00.decode("utf-8")
    # elif isinstance(cell00, unicode):
    #     pass
    if isinstance(cell00, str):
        pass
    elif isinstance(cell00, float):
        if not int(cell00) == cell00:
            msg = "The cell contains a float number " \
                  "but the float isn't an integer"
            raise Exception(msg)
    else:
        msg = "%s, %s"%(repr(cell00), type(cell00))
        raise Exception("Non-recognized type: " + msg)
    if not v:
        return [None, None]
    try:
        lab_no = get_lab_no(v)
    except BaseException as e:
        print(LINE)
        print(row, col)
        # if is_all_numbers(cell00):
        #     print("Number only cell")
        print(e.args[0].encode("utf-8"))
        return [None, None]
    return lab_no

def get_TEXT(sheet, row, col):
    v = get_string(sheet, row, col).lower()
    if not v:
        v = None
    return v
    # s = ""
    # for c in v:
    #     if c.isalnum():
    #         s += c
    #     else:
    #         s += " "
    # return s

def get_DECIMAL(sheet, row, col):
    v = get_string(sheet, row, col)

    value = None
    eq_sign = None
    description = None

    if not v:
        pass
    elif v.count(",") == 1:
        if v.count("."):
            print(row+1, repr(v))
            raise ValueError("Comma and dot together")
        # else:
        v = v.replace(",", ".")
    elif v.count(",") > 2:
        print(row+1, repr(v))
        raise ValueError("Two commas")
    if v.replace(".", "") == "":
        if v.count(".") > 2:
            v = v.replace(".", "")
    try:
        v = v.replace("..", ".")
        v = v.replace(",,", ".")
        value = decimal.Decimal(v).quantize(decimal.Decimal('.001'))
    except (ValueError, decimal.InvalidOperation):
        pattern = "(<|>)\s*([-]?[0-9]+(\.[0-9]+)?)$"
        if v.lower() in ["nema zemlje", "nema uzorka", "nema uzoraka", u"nestalo je zemlji\u0161te", "nije radjeno", "nije radjen", "nije radj.", "nije rađeno", "nije rađen", "nije rađen", "tresetno", "treset", "tresetan", "tresetan uzorak", "t" ,"nije bilo zemlje", "humus", "humusan", "jako humusan", "humusno", "lišće i lišaji", "pijesak", "iglice-lišaji", "skeletno", "?250", "?39.01"]:
            description = v.lower()
        elif v in ["/", "-"]:
            pass
        elif re.match(pattern, v):
            match = re.match(pattern, v)
            eq_sign = match.groups()[0]
            value = match.groups()[1]
        elif v == "":
            pass
        else:
            print("%s%d"%(chr(ord("A") + col), row+1), repr(v))
            raise
    return value, eq_sign, description

def get_PROFILE(sheet, row, col):
    s = get_string(sheet, row, col)
    for c1, c2 in [("б", "b"),
                   ("г", "g"),
                   ("ф", "f"),
                   ("м", "m")]:
        s = s.replace(c1, c2)

    if s.endswith(".0"): # remove the decimal zero added by sqlite
        s = s[:-len(".0")]
    prefix = None
    number = None
    suffix = None
    number2 = None
    suffix2 = None
    res = re.match("([^0-9]*)([0-9]+)(.*)", s)
    if res is not None:
        _prefix = res.groups()[0]
        number = res.groups()[1]
        _rest = res.groups()[2].strip()
        prefix = ""
        for c in sorted([c for c in _prefix]):
            if c.isalnum():
                prefix += c
        res2 = re.match("([^0-9]*)([0-9]*)(.*)", _rest)
        if res2 is not None:
            _suffix = res2.groups()[0]
            number2 = res2.groups()[1]
            _suffix2 = res2.groups()[2]
            suffix = ""
            for c in sorted([c for c in _suffix]):
                if c.isalnum():
                    suffix += c
            suffix2 = ""
            for c in sorted([c for c in _suffix2]):
                if c.isalnum():
                    suffix2 += c
    return s, prefix, number, suffix, number2, suffix2

def get_RANGE(sheet, row, col):
    range_ = get_string(sheet, row, col)
    range_min = None
    range_max = None
    range_description = None

    res = re.match("(([0-9]+([\.,][0-9]*)?)(\s*[\-_—–][\-_—–]?\s*|\s+do\s+))([0-9]+([\.,][0-9]*)?)?(?:cm)?$",
                   range_)
    if res is None:
        if range_:
            res = re.match("([a-d]\)|i+)\s*\w*", range_)
            if res is None and range_:
                if range_ in ["1sloj", "2sloj", "3sloj", "sloj izmedju 1 i 2 sloja", "sloj izmedju 2 i 3 sloja", "duvan", "kukuruz", "paprika"]:
                    range_description = range_
                elif range_ in [
                    "15.vii.1971-radović marica-martinići",
                    '15.ix.1972-uzorci-ams jugoslavija-hotel ,,perazića do"-petrovac',
                ]:
                    raise InvalidRangeException()
                elif re.match("\d+", range_) is None:
                    # the cell contains no number whatsoever so probably an invalid row
                    raise InvalidRangeException()
                else:
                    msg = "%s %s %s"%("%s%d"%(chr(ord("A") + col), row+1), range_, res)
                    raise Exception(msg)
            else:
                range_description = range_
        else:
            raise InvalidRangeException()
    else:
        range_min = res.groups()[1]
        if range_min:
            range_min = range_min.replace(",", ".")
        range_max = res.groups()[4]
        if range_max is not None:
            range_max = range_max.replace(",", ".")
    if range_min and range_max:
        assert float(range_min) < float(range_max)
    return range_min, range_max, range_description

def get_STABILITY(sheet, row, col):
    v = get_string(sheet, row, col)
    for pair in(("stabialn", "stabilan"),
                ("stabilno", "stabilan"),
                ("stabolno", "stabilan"),
                ("stbilan", "stabilan"),
                ("sabilan", "stabilan"),
                ("stabilni", "stabilan"),
                ("stabil.", "stabilan"),
                ("stabil", "stabilan"),
                ("stabilna", "stabilan"),
                ("stab.", "stabilan"),
                ("stabilni.", "stabilan"),
                ("stabil,.", "stabilan"),
                ("stablan", "stabilan"),
                ("potp.nestabilan", "potpuno nestabilan"),
                ("potp.nestab.", "potpuno nestabilan"),
                ("potp.nestab", "potpuno nestabilan"),
                ("potp. nestabilan", "potpuno nestabilan"),
                ("potpno nestabilan", "potpuno nestabilan"),
                ("potpno nestabilno", "potpuno nestabilan"),
                ("potpuno nestabilna", "potpuno nestabilan"),
                ("pot.nestabilan", "potpuno nestabilan"),
                ("potp. nestabilan", "potpuno nestabilan"),
                ("pot.nestab.", "potpuno nestabilan"),
                ("pot.nestabil.", "potpuno nestabilan"),
                ("pot. nestabilan", "potpuno nestabilan"),
                ("pol.nestab.", "potpuno nestabilan"),
                ("pot,nestab", "potpuno nestabilan"),
                ("pot. nest.", "potpuno nestabilan"),
                ("pot.nestab", "potpuno nestabilan"),
                ("potpuno nest.", "potpuno nestabilan"),
                ("potp.nest.", "potpuno nestabilan"),
                ("potpunonestabilan", "potpuno nestabilan"),
                ("pot nest.", "potpuno nestabilan"),
                ("potpuno nestabililan", "potpuno nestabilan"),
                ("pol. nestab.", "potpuno nestabilan"),
                ("pot-nest.", "potpuno nestabilan"),
                ("potp nestabilan", "potpuno nestabilan"),
                ("potpuno nestabilni", "potpuno nestabilan"),
                ("potp.nestabilni", "potpuno nestabilan"),
                ("potp. nestabilni", "potpuno nestabilan"),
                ("pot.nestabilno", "potpuno nestabilan"),
                ("potpuno nestabilno", "potpuno nestabilan"),
                ("pot.nestabilni", "potpuno nestabilan"),
                ("potpunonestabilni", "potpuno nestabilan"),
                ("pot. nestabilna", "potpuno nestabilan"),
                ("potpuno nesabilan", "potpuno nestabilan"),
                ("pot. nestabilna", "potpuno nestabilan"),
                ("potpuno nesabilan", "potpuno nestabilan"),
                ("pot. nestabilam", "potpuno nestabilan"),
                ("pot. nestabilni", "potpuno nestabilan"),
                ("potpuno nestab.", "potpuno nestabilan"),
                ("poptp.nestabil.", "potpuno nestabilan"),
                ("por.nestab", "potpuno nestabilan"),
                ("pot nestab.", "potpuno nestabilan"),
                ("pot. nestab.", "potpuno nestabilan"),
                ("pot.nest.", "potpuno nestabilan"),
                ("potp. nestab", "potpuno nestabilan"),
                ("potp. nestabilna", "potpuno nestabilan"),
                ("potpono nestabilan", "potpuno nestabilan"),
                ("potpuno nestabiulan", "potpuno nestabilan"),
                ("potpuno netabilan", "potpuno nestabilan"),
                ("potpunp nestabilan", "potpuno nestabilan"),
                ("malo stab.", "malo stabilan"),
                ("malo.stab.", "malo stabilan"),
                ("malo stabilni", "malo stabilan"),
                ("malo stabilna", "malo stabilan"),
                ("malo stabilno", "malo stabilan"),
                ("malo stabil.", "malo stabilan"),
                ("malo.stabilni", "malo stabilan"),
                ("malo stabilam", "malo stabilan"),
                ("malo stabolno", "malo stabilan"),
                ("molo stabilan", "malo stabilan"),
                ("buavo", "buav"),
                ("buao", "buav"),
                ("skeletno", "skeletan"),
                ("skelet", "skeletan"),
                ("skeletna", "skeletan"),
                ("nestabilni", "nestabilan"),
                ("nestabil.", "nestabilan"),
                ("nestabilna", "nestabilan"),
                ("nestab.", "nestabilan"),
                ("nestabilno", "nestabilan"),
                ("nesabilni", "nestabilan"),
                ("potpuno stabilni", "potpuno stabilan"),
                ("potpuno stabilno", "potpuno stabilan"),
                ("dosta stabilni", "dosta stabilan"),
                ("dosta stabilno", "dosta stabilan"),
                ("dosta stabilna", "dosta stabilan"),
                ("dosta stab.", "dosta stabilan"),
                ("dosta stabila", "dosta stabilan"),
                ("dosta stab.", "dosta stabilan"),
                ("dosta stabila", "dosta stabilan"),
                ("dosta.stab", "dosta stabilan"),
                ("pjeskovito", "pijesak"),
                ("tresetno", "tresetan"),
                ("treset.", "tresetan"),
                ("nije radjeno", "nije rađen"),
                ("nije radjen", "nije rađen"),
                ("humusno", "humusan"),
                ("pjesak", "pijesak"),
                ("skeletno-buavo", "skeletan-buav"),
                ("skeletno-buao", "skeletan-buav"),
                ("skeletan sitni pijesak", "skeletan i sitni pijesak"),
                ("skeletno sitni pijesak", "skeletan i sitni pijesak"),
                ("skeletno sit.pijesak", "skeletan i sitni pijesak"),
                ("skeletno-humusno", "skeletan-humusan"),
                ("humusno-buavo", "humusan-buav"),
                ("zaslanjeni", "zaslanjen"),
    ):
        if v == pair[0]:
            v = v.replace(pair[0], pair[1])
            break
    # We convert some of the values to ""/null
    for pair in (("-", ""),
                 ("nije rađen", ""),
                 ("nema uzorka", ""),
                ):
        if v == pair[0]:
            v = v.replace(pair[0], pair[1])
            break
    return v, None

def get_HUMUS_CHARACTER(sheet, row, col):
    v = get_string(sheet, row, col)
    for pair in(
                ("neutralno", "neutralan"),
                ("neutralna", "neutralan"),
                ("neutralni", "neutralan"),
                ("neutalan", "neutralan"),
                ("neutralam", "neutralan"),
                ("nautralan", "neutralan"),
                ("nautralno", "neutralan"),
                ("neuralan", "neutralan"),
                ("eutralan", "neutralan"),
                ("neutrlan", "neutralan"),
                ("neurtralan", "neutralan"),
                ("kiselo", "kiseo"),
                ("kisjelo", "kiseo"),
                ("iseo", "kiseo"),
                ("sl.kiseo", "slabo kiseo"),
                ("slabo kiselo", "slabo kiseo"),
                ("sl. kiseo", "slabo kiseo"),
                ("sl.kiselo", "slabo kiseo"),
                ("sl.kiselo", "slabo kiseo"),
                ("sl.kisjelo", "slabo kiseo"),
                ("slabo ksieo", "slabo kiseo"),
                ("slabo kiseli", "slabo kiseo"),
                ("slabo kiso", "slabo kiseo"),
                ("sabo kiseo", "slabo kiseo"),
                ("slabo kieo", "slabo kiseo"),
                ("slabokiseo", "slabo kiseo"),
                ("slbo kiseo", "slabo kiseo"),
                ("sl. kiselo", "slabo kiseo"),
                ("manje kiselo", "manje kiseo"),
                ("m.kiseo", "manje kiseo"),
                ("manje kisjelo", "manje kiseo"),
                ("manje kis.", "manje kiseo"),
                ("maje kiseo", "manje kiseo"),
                ("vr.sl.kiseo", "vrlo slabo kiseo"),
                ("vr.slabo kiseo", "vrlo slabo kiseo"),
                ("vr .slabo kiseo", "vrlo slabo kiseo"),
                ("vrlo slabo kisjelo", "vrlo slabo kiseo"),
                ("vrlo-slabo kiseo", "vrlo slabo kiseo"),
                ("vr. slabo kiseo", "vrlo slabo kiseo"),
                ("vr.slab kiseo", "vrlo slabo kiseo"),
                ("vrlo sl.kiseo", "vrlo slabo kiseo"),
                ("vr .sl. kiseo", "vrlo slabo kiseo"),
                ("vr.sl. kiseo", "vrlo slabo kiseo"),
                ("vr. sl. kiseo", "vrlo slabo kiseo"),
                ("vrlo-sl.kiseo", "vrlo slabo kiseo"),
                ("vr.sl.kiselo", "vrlo slabo kiseo"),
                ("vr.slabo kiselo", "vrlo slabo kiseo"),
                ("vrlo slabo kiselo", "vrlo slabo kiseo"),
                ("tamno smedje", "tamno smeđe"),
    ):
        if v == pair[0]:
            v = v.replace(pair[0], pair[1])
            break
    for pair in(
                ("nema uzorka", ""),
                ("nema zemlje", ""),
               ):
        if v == pair[0]:
            v = v.replace(pair[0], pair[1])
            break
    return v, None

def get_WATER_PERMEABILITY(sheet, row, col):
    v = get_string(sheet, row, col)
    cm = None
    min = None

    for pattern in [r"([0-9]+([\.,][0-9]*)?)\s*/+\s*(30)",
                    ]:
        res = re.match(pattern, v)
        if res is not None:
            break

    if (res is None) and v:
        if v in ["0", "0.0", "0,0"]:
            cm = 0
            min = 30
        elif v not in ["Ø", "ø"]:
            msg = "%s %s %s"%("%s%d"%(chr(ord("A") + col), row+1), v, res)
            raise Exception(msg)
    elif res is None:
        return None#, None
    else:
        cm = res.groups()[0]
        min = res.groups()[2]
    return cm#, min

def get_DORSEY_FACTOR(sheet, row, col):
    v = get_string(sheet, row, col)
    for pair in [("‾", "-"),
                 ("\u207b", "-"),
                 ("\u2074", "4"),
                 ("\u2075", "5"),
                 ("\u2076", "6"),
                 ]:
        v = v.replace(pair[0], pair[1])
    # Match e.g. 1,05*10^(-4)
    res = None
    significand = None
    exponent = None


    for pattern in [r"([0-9]+([\.,][0-9]+)?)\*10\^\(([-]?[0-9]+)\)",
                    # Try to match e.g. 1,05*10-4
                    r"([0-9]+([\.,][0-9]+)?)\*10([-][0-9]+)",
                    # Try to match e.g. 1.9x 10 na -5
                    r"([0-9]+([\.,][0-9]+)?)\s*(?:x|X)\s*10\s*[(]?\s*(?:na)?\s*([-][0-9]+)\s*[)]?",
                    ]:
        res = re.match(pattern, v)
        if res is not None:
            significand = float(res.groups()[0].replace(",", "."))
            exponent = int(res.groups()[2])
            break
    if (res is None) and v:
        if v in ["0", "0.0", "0,0"]:
            significand = 0
            exponent = 0
        elif v not in ["Ø", "ø"]:
            print("OK")
            print(v.encode("utf-8"))
            msg = "%s %s %s"%("%s%d"%(chr(ord("A") + col), row+1), v, res)
            raise Exception(msg)
    if significand is not None and exponent is not None:
        return significand*(10**exponent)
    elif significand is None and exponent is None:
        return None
    else:
        msg = "%s %s"%("%s%d"%(chr(ord("A") + col), row+1), v)
        raise Exception(msg)

def get_SECTION_AND_SQUARE(sheet, row, col):
    v = get_string(sheet, row, col)
    section = None
    square = None
    for pattern in [r"(.*?(?:\d|ljes|lješ))\s*(\(...*\))?",
                    # r"(.*)(.*)"
                    ]:
        res = re.match(pattern, v)
        if res is not None:
            section = section_simplify(res.groups()[0])
            square = res.groups()[1]
            if square is not None:
                square = "".join(square[1:-1].replace("-", "").split())
            break
    if (res is None) and v:
        print("Error")
        print(v.encode("utf-8"))
        msg = "%s %s %s"%("%s%d"%(chr(ord("A") + col), row+1), v, res)
        raise Exception(msg)

    return section, square

def get_SQUARE(sheet, row, col):
    v = get_string(sheet, row, col)
    if v is not None:
        square = v.replace("-", "")
    return square

def get_ZERO_OR_ONE(sheet, row, col):
    v = get_string(sheet, row, col)
    if v.endswith(".0"):
        v = v[:-2]
    try:
        i = int(v)
    except ValueError:
        return None
    if i not in(0, 1):
        msg = "%s %s"%("%s%d"%(chr(ord("A") + col), row+1), v)
        raise Exception(msg)
    return i

def get_SECTION(sheet, row, col):
    v = get_string(sheet, row, col)
    return section_simplify(v)

def get_COORDINATE(sheet, row, col):
    v = get_string(sheet, row, col)
    if v == "":
        v = None
    return v


def parse_row(row, sheet, pfile):

    section = get_section(sheet, row, 0)
    if section:
        return section

    page = get_page(sheet, row, 0)
    if page:
        return page

    lab_no = get_LAB_NO(sheet, row, 0)
    # if lab_no is None:
    #     return


    values = list()
    values.extend(lab_no)
    for i, (c, t) in enumerate(COLUMNS.items()):
        getter = globals()["get_" + t]
        try:
            res = getter(sheet, row, i+1)
        except InvalidRangeException as e:
            # Depth2 is not necessary so we shouldn't skip this row
            if c != "depth2" and lab_no == [None, None]:
                    # if both depth and lab_no are invalid the row is invalid
                    # print("Row %d"%(row+1), end=" ")
                    # print("has an invalid lab_no and depth, skipping...")
                    raise InvalidRowException()
            else:
                range_min = range_max = range_description = None
                res = (range_min, range_max, range_description)

        except:
            print("Row:", row+1)
            raise

        if isinstance(res, (tuple, list)):
            _res = []
            for i in res:
                if i == "":
                    _res.append(None)
                else:
                    _res.append(i)
            res = tuple(_res)
            values.extend(res)
        else:
            if res == "":
                values.append(None)
            else:
                values.append(res)
    # print(values)
    # print(len(values))
    # if values[2:] == [None, None, None]
    return values

def main():
    cli_parser = OptionParser(
        usage='usage: %prog [options] <DIRECTORY> [<output.sqlite>]'
        )
    cli_parser.add_option("-f", "--force", action="store_true",
                      dest="force", default=False,
                      help="Overwrite output file if it already exists.")
    cli_parser.add_option("-b", "--beginpage",
                      dest="beginpage",
                      help="First page number.")
    cli_parser.add_option("-e", "--endpage",
                      dest="endpage",
                      help="Last page number.")
    (options, args) = cli_parser.parse_args()

    if not args:
        cli_parser.error("You have to supply a DIRECTORY")
    elif len(args) > 2:
        cli_parser.error("You can supply at most 2 arguments:\n%s"
                         %cli_parser.usage)
    else:
        dir_ = args[0]#.decode("utf-8")
        if len(args) == 2:
            sqlite_filename = args[1]
        elif len(args) == 1:
            sqlite_filename = dir_ + ".sqlite"
            print("Warning: output SQLite 3 file name not provided. "
                  "Using %s as the output file name...\n"%repr(sqlite_filename))
        else:
            raise Exception
    if os.path.exists(sqlite_filename):
        if options.force:
            os.remove(sqlite_filename)
        else:
            msg = "Output file %s already exists. Use --force (or -f) " \
                  "to overwrite this file."%sqlite_filename
            cli_parser.error(msg)

    convert_dir(dir_, args, options)
    print(LINE)
    print("Finished OK!")

if __name__ == "__main__":
    main()
