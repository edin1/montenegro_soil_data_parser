import os
import sys
import csv
import xlrd
import helpers as h

def get_string(sheet, row, col):
    try:
        cell = sheet.cell(row, col)
    except IndexError:
        return None
        # print(sheet.cell(row, col-1))
        # print(dir(sheet), row, col)
        # raise
    v = cell.value
    if cell.ctype == 3:
        print("%s%d"%(chr(ord("A") + col), row+1), repr(v))
        raise Exception("DATE type found")
    v = str(v).strip()#.lower()
    if v == "":
        return None
    v = " ".join(v.split())
    v = ", ".join([word.strip() for word in v.split(",")])
    return v

def get_row(sheet, row):
    return [get_string(sheet, row, col) for col in range(sheet.ncols)]

def main():
    fname1 = sys.argv[1]
    fname1_abs = os.path.abspath(fname1)
    fname1_base = os.path.basename(fname1_abs)
    fname1_new = fname1_abs + ".csv"

    wb1 = xlrd.open_workbook(fname1_abs)
    sheet1 = wb1.sheets()[0]

    # fname2 = sys.argv[2]
    # fname2_abs = os.path.abspath(fname2)
    # fname2_base = os.path.basename(fname2_abs)

    # wb2 = xlrd.open_workbook(fname2_abs)
    # sheet2 = wb2.sheets()[0]

    # print(dir(sheet))

    rows = []
    for row in range(sheet1.nrows):
        values = get_row(sheet1, row)
        if values[0] not in ["lab. br.", 'lab. Broj.', None]:
            if values[0] != values[19]:
                print(values[0])
                raise Exception("Different heading for row %d."%(row+1))
            values_insert = [None] * sheet1.ncols
            values_insert[1] = values[0]
            values_insert[20] = values[19]
            values[0] = None
            values[19] = None
            rows.append(values_insert)
        rows.append(values)

    with open(fname1_new, 'w', encoding='utf8') as csvfile:
        csvwriter = csv.writer(csvfile, lineterminator='\n')
        for row in rows:
            row_new = [h.myconvert(e) for e in row]
            csvwriter.writerow(row_new)

if __name__ == "__main__":
    main()