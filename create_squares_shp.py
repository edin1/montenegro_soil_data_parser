from collections import defaultdict
import shapefile

X_DELTA = 2000
X_MIN = 6527996.71801
# X_MIN += X_DELTA*21
X_MIN -= X_DELTA
X_MAX = X_MIN + X_DELTA*47

Y_DELTA = 2000
# Y_MIN = 41.7506
Y_MIN = 4636000 # 4635975.516680000349879 #4651283.9018
Y_MIN += Y_DELTA*7
Y_MAX = Y_MIN + Y_DELTA*84

w = shapefile.Writer(shapefile.POLYGON)
w.field("id", "N")
w.field("name", "C")

i = 1
parts = []
records = []
x = X_MIN
while x <= X_MAX:
    y = Y_MIN
    while y <= Y_MAX:
        w.record(i, "square %d"%i)
        polygon = [
            [x, y],
            [x, y + Y_DELTA],
            [x + X_DELTA, y + Y_DELTA],
            [x + X_DELTA, y],
            [x, y],
        ]
        w.poly(parts=[polygon])
        # parts.append(polygon)
        # records.append(str(i))
        y += Y_DELTA
        i += 1
    x += X_DELTA


# w.poly(parts=parts)
# w.record(*records)

w.save('./squares_west_project_crs.shp')
