import os
from pprint import pprint
import sqlite3
import helpers as h

def str2set(s):
    if s.strip() == "":
        raise Exception("Argument cannot be an empty string")
    s_ = ""
    for c in s:
        if not c.isalpha():
            s_ += " "
        else:
            s_ += c
    return set(s_.split())

def mydistance(s1, s2):
    s1 = str2set(s1)
    s2 = str2set(s2)

    if s1 == s2: # same sets
        return 0
    elif s1.intersection(s2) == set(): # completely different
        return -1
    else:
        return len(s1.symmetric_difference(s2))

def fix(cli_parser, args, options):
    dbconn = sqlite3.connect(args[0],
                             detect_types=sqlite3.PARSE_DECLTYPES,
                             check_same_thread=False)
    cursor = dbconn.cursor()
    table = options.table
    types = [i[2] for i in
             cursor.execute("PRAGMA table_info(%s)"%table).fetchall()]
    columns = [i[1] for i in
               cursor.execute("PRAGMA table_info(%s)"%table).fetchall()]

    columns_categories = []
    for i, col in enumerate(columns):
        if col.endswith("_c2n"):
            columns_categories.append(col[:-len("_c2n")])
    col = None

    print(columns_categories)
    for col in columns_categories:
        cmd = """select count(*) _c, %s
                 from properties
                 where %s is not null
                 group by %s
                 order by _c desc;"""%(col, col, col)
        # categories are already sorted by count so we ditch the count
        rows = [row[1] for row in cursor.execute(cmd).fetchall()]
        first = None
        numbers2categories = {}
        # Increment the number below for next category
        category_no = 0
        while rows:
            category = rows.pop(0)
            category_no += 1
            numbers2categories[category_no] = category
            distances = {}
            for s in rows:
                distance = mydistance(category, s)
                if distance == -1:
                    continue
                if distance in distances.keys():
                    distances[distance].append(s)
                else:
                    distances[distance] = [s]
            keys = list(distances.keys())
            while keys:
                key = min(keys)
                keys.remove(key)
                _potential_categories = distances[key]
                while _potential_categories:
                    index = min([rows.index(_c) for _c in _potential_categories])
                    category = rows.pop(index)
                    category_no += 1
                    numbers2categories[category_no] = category
                    _potential_categories.remove(category)
        categories2numbers = dict([(j, i) for i,j in numbers2categories.items()])
        pprint(numbers2categories)
        table = col + "_c2n"
        cursor.execute("DROP TABLE IF EXISTS %s"%table)
        cmd = """CREATE TABLE IF NOT EXISTS %s (
                 category TEXT,
                 number INTEGER)"""%table
        cursor.execute(cmd)
        for number, category in numbers2categories.items():
            cmd = """UPDATE properties
            SET
            %s = %s
            WHERE
            %s = %s;
            """%(col + "_c2n", number, col, repr(category))
            cursor.execute(cmd)
            cmd = """INSERT INTO %s
                (number, category)
                VALUES
                (?, ?)"""%table
            cursor.execute(cmd, [number, category])
    dbconn.commit()
    dbconn.close()

if __name__ == "__main__":
    h.main(handler=fix)