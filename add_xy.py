import sys
import os
import sqlite3

import helpers as h

EXT = ".%s.sqlite"%os.path.splitext(os.path.basename(__file__))[0]

def handler(cli_parser):

    cli_parser.add_option("--ignore-section", action="store_true",
                      dest="ignore_section", default=False,
                      help="Ignore the section (and square) columns.")
    cli_parser.add_option("--save-non-matched-coordinates", action="store_true",
                      dest="save_non_matched_coordinates", default=False,
                      help="Save non matched coordinates to a SQLite file.")
    _cli_parser, args, options= h.parse_cli_parser(cli_parser, EXT)
    columns, types, rows = h.get_columns_rows(args[0], options.table)
    ci = h.get_index(columns)

    dataset_number = h.extract_dataset_number(args[0])
    duplicates = getattr(h, "DATASET%d_PROFILES_DUPLICATE"%dataset_number)

    profile_suffix_section_to_skip = getattr(h, "DATASET%d_PROFILE_SUFFIX_SECTION_TO_SKIP"%dataset_number)
    # print(profile_suffix_section_to_skip)

    # engine_coordinates = sa.create_engine('sqlite:///%s'%args[2], echo=False)
    # metadata = sa.MetaData()
    # Base = sa.ext.declarative.declarative_base()
    # Base.metadata = metadata
    con_coordinates = sqlite3.connect(args[1])

    columns_new = columns[:]
    types_new = types[:]
    columns_new = columns_new[:ci("my_profile_id")] +\
                  ["coordinates_excel_row", "x", "y",
                   "type_1_full_0_empty"] +\
                    columns_new[ci("my_profile_id"):]
    types_new = types_new[:ci("my_profile_id")] +\
                  ["INTEGER", "DECIMAL", "DECIMAL",
                   "ZERO_OR_ONE"] + types_new[ci("my_profile_id"):]
    rows_new = []

    rows_missing_properties = []
    my_profile_id_dict = {}
    coordinates_inserted = {}
    excel_rows_inserted = []
    for row in rows:
        row_new = list(row)
        profile_number = row[ci("profile_number")]
        profile_suffix = row[ci("profile_suffix")]
        my_profile_id = row[ci("my_profile_id")]
        section = row[ci("section")]
        profile_suffix_section = (profile_number, profile_suffix, section)
        # print(profile_suffix_section)
        x2 = row[ci("x2")]
        y2 = row[ci("y2")]
        x2y2 = (x2, y2)
        values = None
        if my_profile_id in my_profile_id_dict:
            values = my_profile_id_dict[my_profile_id]
            # print("Duplicate coordinates for profile: ",
            #       my_profile_id, ". Skipping...")
            # print(row_coordinates)
            # print(rows_new)
            # sys.exit()
        elif None not in x2y2:
            cmd_select_excel_row = """
                    select excel_row, x, y, type_1_full_0_empty
                    from properties
                    WHERE
                    x=%s and
                    y=%s
                """%x2y2
            result = con_coordinates.execute(cmd_select_excel_row).fetchall()
            # print(x2y2)
            # print(result)
            if len(result) == 0:
                if "non_matched" not in args[1]:
                    print(x2y2), args[1]
                    raise Exception("Cannot find these coordinates.")
                # else:
                values = [None]*4
            elif len(result) == 1:
                values = result[0]
            else:
                print(x2y2), args[1]
                raise Exception("More than one points with same coordinates.")


        else:
            if section is None and not options.ignore_section:
                values = [None]*4
            else:
                cmd_select_coordinates = """
                    select excel_row, x, y, type_1_full_0_empty
                    from properties
                """
                _values = []
                if section is None: # and options.ignore_section:
                    cmd_select_coordinates += "where 1=1"
                else:
                    cmd_select_coordinates += "where section2=?"
                    _values.append(section)
                profile_col_names = ["profile_prefix", "profile_number", "profile_suffix",
                    "profile_number2_number", "profile_suffix2"]
                for col_name in profile_col_names:
                    col = row[ci(col_name)]
                    if col is None:
                        cmd_select_coordinates += " and %s is ?"%col_name
                    else:
                        cmd_select_coordinates += " and %s=?"%col_name
                    _values.append(col)
                # print(_values)
                # print(cmd_select_coordinates)
                row_coordinates = con_coordinates.execute(cmd_select_coordinates,
                                               _values).fetchall()
                # print(row[ci("excel_row")], row[ci("page")], row[ci("lab_no")], row[ci("page_row")],
                #     row[ci("profile")])
                # h.pprint(row_coordinates)
                if len(row_coordinates) < 1:
                    # msg = "No coordinates matching this row"
                    # print(msg)
                    rows_missing_properties.append(row)
                    values = [None]*4
                elif len(row_coordinates) > 1:
                    # print(row)
                    for _i in row_coordinates:
                        print(my_profile_id, _i[1], _i[2])
                        print(_i[1], _i[2], sep="\t")
                    print(profile_suffix_section, ",", row[ci("location")], row[ci("square")])
                    print(row_coordinates)
                    msg = "More than one coordinate matching this row"
                    print(msg)
                    # raise Exception(msg)
                    if options.ignore_section:
                        values = [None]*4
                    elif profile_suffix_section not in profile_suffix_section_to_skip:
                        print("python show_my_profile_id.py %s %d"%(args[0], my_profile_id))
                        # h.show_my_profile_id(args[0], my_profile_id)
                        raise Exception(msg)
                    else:
                        print("Ignoring,", profile_suffix_section)
                        values = [None]*4
                else:
                    # print("One row matched")
                    if profile_suffix_section in [
                            (138, None, 'cetinje2'), # The coordinates, MP_3 data and C_3 data: all differ in a weird way
                            (140, None, 'cetinje2'), # The coordinates, MP_3 data and C_3 data: all differ in a weird way
                    ]:
                        values = (None,)*4
                    else:
                        values = row_coordinates[0]
                        key = tuple(_values) + values
                        if key in coordinates_inserted:
                            if profile_suffix_section + (values[1], values[2]) not in duplicates:
                                msg = "Duplicate coordinates for my_profile_id: %d. "%my_profile_id
                                msg += "These coordinates were already matched with "
                                msg += "my_profile_id: %d."%coordinates_inserted[key]
                                print(profile_suffix_section + (values[1], values[2]))
                                raise Exception(msg)
                            else:
                                continue
                        else:
                            coordinates_inserted[key] = my_profile_id
        if my_profile_id not in my_profile_id_dict:
            my_profile_id_dict[my_profile_id] = values
        if values[0] is not None:
            excel_rows_inserted.append(values[0])
        row_new = row_new[:ci("my_profile_id")] + list(values) + \
                      row_new[ci("my_profile_id"):]
        rows_new.append(row_new)
    excel_rows_inserted.sort()
    # h.pprint(excel_rows_inserted)

    columns_coordinates, types_coordinates, rows_coordinates = h.get_columns_rows(args[1], options.table)
    ci_coordinates = columns_coordinates.index
    rows_coordinates_non_matched = []
    for row in rows_coordinates:
        if row[ci_coordinates("excel_row")] not in excel_rows_inserted:
            rows_coordinates_non_matched.append(row)
    print("Creating dbs...")
    h.create_db(options.outfile, "properties",
        list(zip(columns_new, types_new)), rows_new)
    if options.save_non_matched_coordinates:
        h.create_db("non_matched_coordinates.sqlite", "properties",
            list(zip(columns_coordinates, types_coordinates)), rows_coordinates_non_matched)
    # h.create_db(options.outfile.rsplit(".")[0] + ".missing.sqlite", "properties",
    #     list(zip(columns, types)), rows_missing_properties)

if __name__ == "__main__":
    h.main(ext=EXT,
           handler=handler, parse_args=False)
