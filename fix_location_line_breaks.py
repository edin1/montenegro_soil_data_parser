import os
import sys
import csv
import xlrd
import helpers as h

def parse_row(row, sheet):
    pass

def get_string(sheet, row, col):
    try:
        cell = sheet.cell(row, col)
    except IndexError:
        return None
        # print(sheet.cell(row, col-1))
        # print(dir(sheet), row, col)
        # raise
    v = cell.value
    if cell.ctype == 3:
        print("%s%d"%(chr(ord("A") + col), row+1), repr(v))
        raise Exception("DATE type found")
    v = str(v).strip()#.lower()
    if v == "":
        return None
    v = " ".join(v.split())
    return v

def main():
    fname = sys.argv[1]
    fname_abs = os.path.abspath(fname)
    fname_base = os.path.basename(fname_abs)

    fname_new, ext = os.path.splitext(fname_abs)
    fname_new += "_new.csv"

    wb = xlrd.open_workbook(fname_abs)
    sheet = wb.sheets()[0]

    # print(dir(sheet))

    rows = []
    rows_for_profile_previous = []
    profile_previous = None
    for row in range(sheet.nrows):
        location = get_string(sheet, row, 0)
        profile = get_string(sheet, row, 1)
        if profile_previous is None:
            if profile not in [None, 'profil']:
                profile_previous = profile
                rows_for_profile_previous = [[location, profile]]
            else:
                rows.append([location, profile])
        else:
            if profile not in [None]:
                rows.extend(rows_for_profile_previous)
                profile_previous = profile
                rows_for_profile_previous = [[location, profile]]
            else:
                if location not in [None]:
                    rows_for_profile_previous[0][0] += ", " + location
                rows_for_profile_previous.append([None, None])
    rows.extend(rows_for_profile_previous)

    with open(fname_new, 'w', encoding='utf8') as csvfile:
        csvwriter = csv.writer(csvfile, lineterminator='\n')
        for row in rows:
            row_new = [h.myconvert(e) for e in row]
            csvwriter.writerow(row_new)


if __name__ == "__main__":
    main()