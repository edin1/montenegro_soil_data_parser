import os
import sys
import csv
import xlrd
import helpers as h

def get_string(sheet, row, col):
    try:
        cell = sheet.cell(row, col)
    except IndexError:
        return None
        # print(sheet.cell(row, col-1))
        # print(dir(sheet), row, col)
        # raise
    v = cell.value
    if cell.ctype == 3:
        print("%s%d"%(chr(ord("A") + col), row+1), repr(v))
        raise Exception("DATE type found")
    v = str(v).strip().lower()
    if v == "":
        return None
    v = " ".join(v.split())
    v = ", ".join([word.strip() for word in v.split(",")])
    return v

def main():
    fname1 = sys.argv[1]
    fname1_abs = os.path.abspath(fname1)
    fname1_base = os.path.basename(fname1_abs)

    fname2 = sys.argv[2]
    fname2_abs = os.path.abspath(fname2)
    fname2_base = os.path.basename(fname2_abs)

    wb1 = xlrd.open_workbook(fname1_abs)
    sheet1 = wb1.sheets()[0]

    wb2 = xlrd.open_workbook(fname2_abs)
    sheet2 = wb2.sheets()[0]

    # print(dir(sheet))

    row1 = 2
    row2 = 2
    while True:
        lab_no1 = get_string(sheet1, row1+1, 0)
        if lab_no1 and lab_no1.startswith("str "):
            row1 += 1
            continue
        lab_no2 = get_string(sheet2, row2+1, 0)
        if lab_no2 and lab_no2.startswith("str "):
            row2 += 1
            continue

        row1 += 1
        location1 = get_string(sheet1, row1, 1)
        profile1 = get_string(sheet1, row1, 2)
        depth1 = get_string(sheet1, row1, 3)

        row2 += 1
        location2 = get_string(sheet2, row2, 1)
        profile2 = get_string(sheet2, row2, 2)
        depth2 = get_string(sheet2, row2, 3)

        if location1 != location2:
            raise Exception("Different location for rows %d, %d."%(row1+1, row2+1))
        if profile1 != profile2:
            raise Exception("Different profile for row %d, %d."%(row1+1, row2+1))
        if depth1 != depth2:
            raise Exception("Different depth for row %d, %d."%(row1+1, row2+1))

if __name__ == "__main__":
    main()