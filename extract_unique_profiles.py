import sys
import os
from pprint import pprint
import sqlite3

import helpers as h

def handler(cli_parser, args, options):
    columns, types, rows = h.get_columns_rows(args[0], options.table)
    ci = h.get_index(columns)

    con = sqlite3.connect(args[0])

    rows_unique_and_nonunique = con.execute("select distinct MP_my_profile_id, MP_profile_number, MP_profile_suffix from properties").fetchall()
    profile_numbers_unique = con.execute("select distinct MP_profile_number, MP_profile_suffix from properties").fetchall()

    my_profile_id_non_unique = []

    for row in rows_unique_and_nonunique:
        key = (row[1:])
        if key in profile_numbers_unique:
            profile_numbers_unique.remove(key)
        else:
            my_profile_id_non_unique.append(row[0])
    rows_unique = []
    rows_non_unique = []
    for row in rows:
        if row[ci("my_profile_id")] not in my_profile_id_non_unique:
            rows_unique.append(row)
        else:
            rows_non_unique.append(row)
    # h.pprint(rows_non_unique)
    print("Creating dbs...")
    h.create_db(options.outfile, "properties", list(zip(columns, types)), rows_unique)
    h.create_db(options.outfile.replace(".unique.", ".nonunique."), "properties", list(zip(columns, types)), rows_non_unique)

if __name__ == "__main__":
    h.main(ext=".unique.sqlite", handler=handler)
