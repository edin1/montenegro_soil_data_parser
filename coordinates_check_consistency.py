import os
from collections import OrderedDict as OD
import xlrd

import helpers as h
from parse_chemical import get_PROFILE

def handler(cli_parser, args, options):
    wb = xlrd.open_workbook(args[0])
    sheet = wb.sheets()[0]
    profile_tuples = OD()
    xy_dict = OD()
    for row in range(sheet.nrows):
        def get_cell(col):
            cell = sheet.cell(row, col)
            cell = str(cell.value).strip()
            if cell == "":
                cell = None
            return cell
        def get_row():
            return [get_cell(col) for col in range(sheet.ncols)]

        if row < 1:
            ci = h.get_index(get_row())
            continue

        def get_profile():
            cell = get_cell(ci("profile"))
            res = None
            if cell is not None:
                res = ""
                for c in cell.lower():
                    if c.isalnum():
                        res += c
            if res == "":
                res = None
            return res

        if get_cell(ci("problem")) in ["Empty profile"]:#, "Duplicate profile"]:
            continue
        profile = get_profile()
        x = get_cell(ci("x"))
        y = get_cell(ci("y"))
        xy = (x, y)
        # profile_tuple = (get_PROFILE(sheet, row, ci("profile")),
        #                  get_cell(ci("section")), get_cell(ci("square")),
        #                  x, y)

        if None in xy:
            raise Exception("x or y empty, row: %d"%(row+1))
        elif xy in xy_dict:
            print(xy)
            _row = xy_dict[xy] + 1
            msg = "(x, y) for profile (row %d) was already found in row: %d"%(row+1, _row)
            raise Exception(msg)
        else:
            xy_dict[xy] = row

        # if profile is None:
        #     raise Exception("Old profile is None, row: %d"%(row+1))
        # elif profile_tuple in profile_tuples:
        #     print(profile_tuple)
        #     _row = profile_tuples[profile_tuple] + 1
        #     msg = "New profile (row %d) was already found in row: %d"%(row+1, _row)
        #     raise Exception(msg)
        # else:
        #     profile_tuples[profile_tuple] = row


if __name__ == "__main__":
    h.main(ext=".%s.kml"%os.path.splitext(os.path.basename(__file__))[0],
           handler=handler)