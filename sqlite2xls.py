import helpers as h

def sqlite2xls(cli_parser, args, options):
    import xlwt

    w = xlwt.Workbook()
    ws = w.add_sheet('Data')

    if options.sql:
        columns, types, rows = h.get_columns_rows_for_sql(args[0], options.sql)
    else:
        columns, types, rows = h.get_columns_rows(args[0], options.table)



    r = 0
    for c, name in enumerate(columns):
        ws.write(r, c, name)
    for row in rows:
        r += 1
        for c, value in enumerate(row):
            ws.write(r, c, value)
    w.save(options.outfile)

if __name__ == "__main__":
    h.main(ext=".xls", handler=sqlite2xls)