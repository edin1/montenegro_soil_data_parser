import sqlite3
import helpers as h

def fix(cli_parser, args, options):
    cli_parser.usage += " <my_profile_id>"
    if len(args) != 2:
        cli_parser.error("You have to supply exactly 2 arguments")
    # else:
    my_profile_id = args[1]

    h.show_my_profile_id(args[0], my_profile_id)

if __name__ == "__main__":
    h.main(handler=fix)