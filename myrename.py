# -*- coding: utf-8 -*-
import sys
import os
import re
import sqlite3
import decimal
from optparse import OptionParser
from collections  import OrderedDict as OD

def myrenamedir(dir_):
    for file_ in os.listdir(dir_):
        file_ = os.path.abspath(os.path.join(dir_, file_))
        if os.path.isfile(file_):
            myrenamefile(file_)


def myrenamefile(file_):
    _dir = os.path.dirname(file_)
    pfile = file_.encode("utf-8")
    print(pfile)
    basename, ext = os.path.basename(file_).rsplit(".", 1)
    if ext.lower() not in ('xls', 'xlsx'):
        return
    # else:
    student, book, map_part, pages = basename.split(", ")
    page_begin, page_end = pages.split()[1].split('-')
    page_begin = "%03d"%int(page_begin)
    page_end = "%03d"%int(page_end)
    newfile = os.path.join(_dir, "%s, str %s-%s, %s.%s"%(book, page_begin, page_end, student, ext))
    os.rename(file_, newfile)

def main():
    cli_parser = OptionParser(
        usage='usage: %prog [options] <DIRECTORY>'
        )
    cli_parser.add_option("-f", "--force", action="store_true",
                      dest="force", default=False,
                      help="Overwrite output file if it already exists.")
    (options, args) = cli_parser.parse_args()

    if not args:
        cli_parser.error("You have to supply a DIRECTORY")
    elif len(args) > 1:
        cli_parser.error("You can supply at most 1 arguments:\n%s"
                         %cli_parser.usage)
    else:
        dir_ = args[0]#.decode("utf-8")
        myrenamedir(dir_)

if __name__ == "__main__":
    main()