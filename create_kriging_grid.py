import csv
import numpy as np

import helpers as h
import add_dem as dem

KEYS = dem.KEYS[:]

GLS1975_template = "/media/Evo850-1TB-1/edin/afh/me_soil_tables/GLS1975_Edin_CRS_p201r030_3dm19780706_z34_%d0.tif"
GLS1975 = [GLS1975_template%i for i in range(1,5)]

GLS1990_template = "/media/Evo850-1TB-1/edin/afh/me_soil_tables/GLS1990_Edin_CRS_p187r031_5dt19870724_z34_%d0.tif"
GLS1990 = [GLS1990_template%i for i in range(1,8)]

KEYS_GLS = ["GLS1975_band%d"%i for i in range(1, 5)] + ["GLS1990_band%d"%i for i in range(1, 8)]
KEYS.extend(KEYS_GLS)

N = 100 # The number for downsampling of dots

# Border of the region where mapping of phosphorus was performed, inside section Cetinje 1
x_min = 6569639 # int(min(X))
x_max = 6588163 # int(max(X))
y_min = 4683447 # int(min(Y))
y_max = 4704582 # int(max(Y))

# x_min = 6564952 # int(min(X))
# x_max = 6587624 # int(max(X))

# x_min = int(min(X))
# x_max = int(max(X))

# y_min = 4684059 # int(min(Y))
# y_max = 4715465 # int(max(Y))

# y_min = int(min(Y))
# y_max = int(max(Y))

# Ovo je granica oblasti u kontinentalnom dijelu koja
# obuhvata više sekcija. Slabi prognostički rezultati
# x_min = 6608698
# y_max = 4776208

# x_max = 6670694
# y_min = 4725904

# Oblast u okviru Trebinje 2, dosta jednačito zemljište
# x_min = 6548119
# y_max = 4761686

# x_max = 6568400
# y_min = 4734355

with open("map-range.tex", "w") as f:
    f.write(r"x_{min} & = & %d\\"%x_min + "\n")
    f.write(r"x_{max} & = & %d\\"%x_max + "\n")
    f.write(r"y_{min} & = & %d\\"%y_min + "\n")
    f.write(r"y_{max} & = & %d\\"%y_max + "\n")

def get_auxiliary_list(x, y):
    auxiliary_list = list()
    for key in dem.KEYS:
        auxiliary_list.append(dem.get_dem(key, x, y))
    for fname in GLS1975:
        auxiliary_list.append(dem.get_value(fname, x, y))
    for fname in GLS1990:
        auxiliary_list.append(dem.get_value(fname, x, y))
    return auxiliary_list

def create_kriging_grid(cli_parser, args, options):
    with open(args[0], 'r', encoding='utf8') as csvfile:
        reader = csv.reader(csvfile, lineterminator='\n')
        columns = next(reader)
        columns.extend(KEYS)
        ci = columns.index
        rows = []
        for row in reader:
            row_new = list(row)
            row_new[ci("x")] = float(row_new[ci("x")])
            row_new[ci("y")] = float(row_new[ci("y")])
            _row_new = []
            for i, e in enumerate(row_new):
                if not isinstance(e, (int, float)):
                    try:
                        e = float(e)
                    except ValueError:
                        print(e)
                        pass
                _row_new.append(e)
            rows.append(_row_new)
    rows = np.array(rows)
    X = rows[:, ci("x")]
    Y = rows[:, ci("y")]

   # Normalization/whitening
    for c in columns:
        if c in ["x", "y", "MP_my_profile_id"] + KEYS:
            continue
        # else:
        #     rows[:, ci(c)] = rows[:, ci(c)] - rows[:, ci(c)].mean()
        #     rows[:, ci(c)] = rows[:, ci(c)]/\
        #                      (max(rows[:, ci(c)]) - min(rows[:, ci(c)]))

    with open(options.outfile, 'w', encoding='utf8') as csvfile:
        csvwriter = csv.writer(csvfile, lineterminator='\n')
        csvwriter.writerow(columns)
        for row in rows:
            # print(row[ci("x")])
            if x_min <= row[ci("x")] <= x_max and \
                y_min <= row[ci("y")] <= y_max:
                # print(row)
                csvwriter.writerow(list(row) + get_auxiliary_list(row[ci("x")], row[ci("y")]))

    data_kriging = []
    for x in range(x_min, x_max, N):
        for y in range(y_min, y_max, N):
            data_kriging.append([x, y] + get_auxiliary_list(x, y))

    with open(args[0].rsplit(".", 1)[0] + ".grid.csv",
              'w', encoding='utf8') as csvfile:
        csvwriter = csv.writer(csvfile, lineterminator='\n')
        csvwriter.writerow(["x", "y"] + KEYS)
        for row in data_kriging:
            csvwriter.writerow(row)

if __name__ == "__main__":
    h.main(ext=".data.csv", handler=create_kriging_grid)
