import os
import sys
import sqlite3
import csv
import helpers as h

COLUMNS = ["SQLite file", "table", "Total profiles",
    "Total rows/horizons"]

CELLS = []

def main():
    fname = sys.argv[0].rsplit(".", 1)[0] + ".csv"
    with open(fname, 'w', encoding='utf8') as csvfile:
        csvwriter = csv.writer(csvfile)
        process(csvwriter)
def process(csvwriter):
    csvwriter.writerow(COLUMNS)
    def myprint(*elements):
        global CELLS
        CELLS.extend(elements)
        if len(CELLS) == len(COLUMNS):
            csvwriter.writerow(CELLS)
            for c in CELLS:
                print(c)
            CELLS = []

    for f, t in [
        ("MP_1235.fix_profile.sqlite", "properties"),
        ("C_1235.fix_profile.sqlite", "properties"),
        ("MP_C_.sqlite", "properties"),
        ("MP_C_.final.sqlite", "properties"),
        ("MP_C_.final.sqlite", "xy_view"),
        # ("CMP.sqlite", "properties"),
        # ("Coordinates.fix_profile.sqlite", "properties"),
        # ("MPC.add_coordinates.sqlite", "coordinates_view"),
        # ("Coordinates.missing.sqlite", "properties"),
    ]:
        con = sqlite3.connect(f, detect_types=sqlite3.PARSE_DECLTYPES,
                                 check_same_thread=False)
        types = [i[2] for i in
            con.execute("PRAGMA table_info(%s)"%t).fetchall()]
        columns = [i[1] for i in
            con.execute("PRAGMA table_info(%s)"%t).fetchall()]

        ci = h.get_index(columns)

        myprint(f, t)
        print("Total number of profiles")
        cmd = """
        SELECT count(*) from (select distinct %s from %s)
        """%(columns[ci("my_profile_id")], t)
        myprint(con.execute(cmd).fetchone()[0])

        print("Total number of rows/horizons")
        cmd = """
        SELECT count(*) from %s
        """%t
        myprint(con.execute(cmd).fetchone()[0])
        print()
        con.close()

if __name__ == "__main__":
    main()