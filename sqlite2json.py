import json
import sqlite3
import helpers as h

def sqlite2json(cli_parser, args, options):
    if options.sql is None:
        columns, types, rows = h.get_columns_rows(args[0], options.table)
    else:
        con = sqlite3.connect(args[0])
        cur = con.cursor()
        cur.execute(options.sql)
        columns = [e[0] for e in cur.description]
        rows = cur

    if options.columns is not None:
        columns_new = options.columns.split()
        for c in columns_new:
            if c not in columns:
                cli_parser.error("You supplied a nonexistent column: " + c)
        ci = columns.index
        def my_iterator():
            for row in rows:
                _row = [row[ci(c)] for c in columns_new]
                if all(_row):
                    yield(_row)
        rows_new = my_iterator()
    else:
        columns_new = columns
        rows_new = rows

    with open(options.outfile, 'w', encoding='utf8') as jsonfile:
        json.dump(list(rows_new), jsonfile, indent=4)

if __name__ == "__main__":
    h.main(ext=".json", handler=sqlite2json)
