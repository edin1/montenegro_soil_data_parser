import os
import sqlite3
import helpers as h

def fix(cli_parser, args, options):
    columns, types, rows = h.get_columns_rows(args[0], options.table)
    ci = h.get_index(columns)

    columns_new = columns[:]
    columns_new.insert(ci("profile"), "my_profile_id")
    ci_new = columns_new.index
    types_new = types[:]
    types_new.insert(ci("profile"), "INTEGER")

    # print(" OR ".join(["%s like '%%\"%%'"%c for c in columns]))
    # print(" OR ".join(["%s like '%%?%%'"%c for c in columns]))

    dbconn = sqlite3.connect(options.outfile,
                             detect_types=sqlite3.PARSE_DECLTYPES,
                             check_same_thread=False)
    #dbconn.text_factory = str
    cursor = dbconn.cursor()

    cursor.execute("DROP TABLE IF EXISTS properties")

    cmd = """CREATE TABLE IF NOT EXISTS properties(
            %s
    )
    """%(",\n".join(["%s %s"%(c, t) for c, t in zip(columns_new, types_new)]))
    # print(cmd)
    cursor.execute(cmd)

    cmd = """INSERT INTO properties
                (%s)
                VALUES
                (%s)"""%(", ".join(columns_new), ", ".join(["?"]*len(columns_new)))

    old_row = [None]*len(columns)
    old_row[ci("depth_min")] = 0 # Initialize to zero because of comparison
    my_profile_id = 0
    for row in rows:
        new_row = list(row)
        if new_row[ci("profile")] in [None, "-ii-"]:
            # if new_row[ci("book")] == 'Hemijske analize 2' and new_row[ci("student")] == 'Ivana Lalićević' and new_row[ci("page")] == "051" and new_row[ci("excel_row")] in [80, 81]:
            #     old_row = new_row
            #     continue

            # Check the depth parameters of the new_row. If the new
            # depth is smaller of the old depth, that should indicate
            # a new profile
            if old_row[ci_new("depth_min")] is not None and new_row[ci("depth_min")] is not None and int(old_row[ci_new("depth_min")]) > int(new_row[ci("depth_min")]):
                    print(new_row)
                    print(old_row)
                    raise Exception("Old depth is greater than new. "
                                    "This should be a new profile field, "
                                    "but it's not.")
            # Same profile number. Copy the data from the previous row
            # need this line for matching lengths of both rows
            new_row.insert(ci("profile"), my_profile_id)
            new_row[ci_new("my_profile_id"):ci_new("my_profile_id") + 7] = old_row[ci_new("my_profile_id"):ci_new("my_profile_id") + 7]
            new_row[ci_new("section")] = old_row[ci_new("section")]
            new_row[ci_new("section2")] = old_row[ci_new("section2")]
            new_row[ci_new("square")] = old_row[ci_new("square")]
        else:
            if old_row[ci_new("profile_prefix"):ci_new("profile_prefix")+5] != new_row[ci("profile_prefix"):ci("profile_prefix")+5]:
                my_profile_id += 1
            new_row.insert(ci("profile"), my_profile_id)
        cursor.execute(cmd, new_row)
        old_row = new_row
    dbconn.commit()
    dbconn.close()


if __name__ == "__main__":
    h.main(ext=".%s.sqlite"%os.path.splitext(os.path.basename(__file__))[0], handler=fix)
