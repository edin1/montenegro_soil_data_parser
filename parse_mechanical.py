import parse_chemical

COLUMNS = parse_chemical.OD([
        ("location", "TEXT"),
        ("profile", "PROFILE"),
        ("depth", "RANGE"),
        ("size2000um_gt", "DECIMAL"),
        ("size2000_0250um", "DECIMAL"),
        ("size0250_0020um", "DECIMAL"),
        ("size0020_0002um", "DECIMAL"),
        ("size0002um_lt", "DECIMAL"),
        ("sand", "DECIMAL"),
        ("clay", "DECIMAL"),
        ("stability", "STABILITY"),
        ("humidity", "DECIMAL"),
        ("depth2", "RANGE"),
        ("fact_spec_weight", "DECIMAL"),
        ("vol_spec_weight", "DECIMAL"),
        ("porosity", "DECIMAL"),
        ("retention_capacity", "DECIMAL"),
        ("air_capacity", "DECIMAL"),
        ("water_permeability", "WATER_PERMEABILITY"),
        ("Dorsey_factor", "DORSEY_FACTOR"),
        ("contraction_coefficient", "DECIMAL"),
        ("section2", "SECTION"),
        ("square", "SQUARE"),
        ("dry_matter", "DECIMAL"),
        ("organic_matter", "DECIMAL"),
        ("porosity_loss", "DECIMAL"),
        ("mineral_residue", "DECIMAL"),
        ("immediate_humidity", "DECIMAL"),
        ("volume", "DECIMAL"),
        ("Other", "TEXT"),
        ("water_capacity", "DECIMAL"),
        ("x2", "COORDINATE"),
        ("y2", "COORDINATE"),
    ])


if __name__ == "__main__":
    parse_chemical.COLUMNS = COLUMNS
    parse_chemical.main()
