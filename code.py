import sys
import os
import platform
from pprint import pprint
import sqlite3

from osgeo import gdal
gdal.UseExceptions()
from affine import Affine

import helpers as h


# DS = gdal.Open(r"e:\Data\edin\afh\me_soil_tables\Edin_30n000e_20101117_gmted_mea075.tif")
DEM_FNAME = h.mypath(r"e:\Data\edin\afh\me_soil_tables\Montenegro_K34_Edin_CRS_DEM_%s.tif")
KEYS = ["elevation", "slope", "aspect", "plan_curvature"]
ARRAYS = {}

def get_dem(key, x, y):
    if key not in ARRAYS:
        ds = gdal.Open(DEM_FNAME%key)
        band = ds.GetRasterBand(1)
        array = band.ReadAsArray()
        T0 = Affine.from_gdal(*ds.GetGeoTransform())
        T = T0 * Affine.translation(0.5, 0.5)
        ARRAYS[key] = (array, T)
    else:
        array, T = ARRAYS[key]
    xpix, ypix = [round(i) for i in (x, y)*~T]
    return array[(ypix, xpix)]
    # print(elevation)


def handler(cli_parser, args, options):
    columns, types, rows = h.get_columns_rows(args[0], options.table)
    ci = columns.index

    # engine_coordinates = sa.create_engine('sqlite:///%s'%args[2], echo=False)
    # metadata = sa.MetaData()
    # Base = sa.ext.declarative.declarative_base()
    # Base.metadata = metadata

    columns_new = columns[:]
    types_new = types[:]
    columns_new = columns_new[:ci("y")+1] + KEYS + \
                  columns_new[ci("y")+1:]
    types_new = types_new[:ci("y")+1] + ["DECIMAL"]*len(KEYS) + types_new[ci("y")+1:]
    rows_new = []

    rows_missing_properties = []
    my_profile_id_dict = {}
    coordinates_inserted = {}
    for row in rows:
        row_new = list(row)
        x = row[ci("x")]
        y = row[ci("y")]
        row_addition = []
        if x is not None and y is not None:
            x = float(x)
            y = float(y)
            for key in KEYS:
                row_addition.append(float(get_dem(key, x, y)))
        else:
            row_addition = [None]*len(KEYS)
        row_new = row_new[:ci("y")+1] + row_addition + \
                  row_new[ci("y")+1:]
        rows_new.append(row_new)
    print("Creating dbs...")
    h.create_db(options.outfile, "properties",
        list(zip(columns_new, types_new)), rows_new)
    # h.create_db(options.outfile.rsplit(".")[0] + ".missing.sqlite", "properties",
    #     list(zip(columns, types)), rows_missing_properties)

if __name__ == "__main__":
    h.main(ext=".%s.sqlite"%os.path.splitext(os.path.basename(__file__))[0],
           handler=handler)