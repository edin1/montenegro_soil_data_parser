import sys
import os
from pprint import pprint
import sqlite3

import helpers as h

def handler(cli_parser, args, options):
    columns, types, rows = h.get_columns_rows(args[0], options.table)
    ci = h.get_index(columns)

    con = sqlite3.connect(args[0])

    rows_unique_and_nonunique = con.execute("""
select section, square, profile_prefix, profile_number, profile_suffix,
profile_number2_number, profile_suffix2 from (select distinct my_profile_id, section, square, profile_prefix, profile_number, profile_suffix,
profile_number2_number, profile_suffix2 from properties where section is not NULL)""").fetchall()

    print(len(rows_unique_and_nonunique))
    rows_nonunique = rows_unique_and_nonunique[:]
    for row in set(rows_unique_and_nonunique):
        rows_nonunique.remove(row)
    h.pprint(rows_nonunique)
    h.pprint(len(rows_nonunique))

if __name__ == "__main__":
    h.main(ext=".unique.sqlite", handler=handler)
